<?php

namespace App\Providers;

use App\Models\HomePage;
use App\Models\PageModel;
use App\Models\PictureModel;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            
            $view->with('data', HomePage::first())
            ->with('navPages', PageModel::all()->where('idPage', '<>', 1)->where('idPage', '<>', 2));
            
        });
    }
}
