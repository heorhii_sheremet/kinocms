<?php

namespace App\Models;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class HallModel extends Model
{
    use HasFactory;

    protected $table = "Halls";

    protected $primaryKey = 'idHall';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'number',
        'description',
        'scheme',
        'topBanner',
        'idSEOBlock',
        'idCinema',
    ];


    public static function store(Request $request){
        
        $hall = $request->except(['slides', 'SEOblock']);
        //dd($hall);
        if(isset($hall['scheme'])) $hall['scheme'] = HallModel::saveScheme($hall['scheme']);
        if(isset($hall['banner'])) $hall['topBanner'] = HallModel::saveBanner($hall['banner']);

        if($request->SEOblock !== null) {
            $SEOBlock = $request->SEOblock;
            $hall['idSEOBlock'] = SEOBlockModel::create($SEOBlock)->idSEOBlock;
        }
        
        $hall = HallModel::create($hall);

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $hall->idHall;
            PictureModel::saveSlideHall($slide);
        }
        
        return $hall;

    }

    public static function edit($id) {
        return HallModel::with(['SEOBlock', 'pictures'])->find($id);
    }

    public static function updateHall(Request $request, $id) {

        $hallModel = HallModel::with(['SEOBlock', 'pictures'])->find($id);
        
        $hallRequst = $request->except(['slides', 'SEOblock']);
        
        if(isset($hallRequst['scheme'])) {
            HallModel::deleteScheme($id);
            $hallRequst['scheme'] = HallModel::saveScheme($hallRequst['scheme']);
        }
        if(isset($hallRequst['banner'])) {
            HallModel::deleteBanner($id);
            $hallRequst['topBanner'] = HallModel::saveBanner($hallRequst['banner']);
        }

        if(null != $request->only('deleteScheme')) HallModel::deleteScheme($id);
        if(null != $request->only('deleteBanner')) HallModel::deleteBanner($id);

        if($request->SEOblock !== null) {
            $SEOBlock = $request->SEOblock;
            SEOBlockModel::find($hallModel->SEOBlock->idSEOBlock)->update($SEOBlock);
        }
        
        HallModel::find($id)->update($hallRequst);

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $id;
            PictureModel::saveSlideHall($slide);
        }
    }

    public static function destroyHall($id) {

        HallModel::deleteScheme($id);
        HallModel::deleteBanner($id);

        $hall = HallModel::with(['pictures'])->find($id);

        $idCinema = $hall->idCinema;

        foreach($hall->pictures as $picture) {
            PictureModel::deleteSlide($picture->idPicture);
        }
        $hall->delete();

        return $idCinema;

    }

    public static function getTodaySessionById(int $id){

        $sessionsToday = SessionModel::where('date', 'like', date('Y-m-d') .'%')
        ->where('idHall', $id)
        ->get();
       
        return $sessionsToday;
    }

    private static function saveScheme($img) {
        return HallModel::saveImg($img, 'scheme/halls');
    }

    private static function saveBanner($img) {
        return HallModel::saveImg($img, 'banner/halls');
    }

    private static function deleteScheme($id) {
        return HallModel::deleteImg($id, 'scheme');
    }

    private static function deleteBanner($id) {
        return HallModel::deleteImg($id, 'topBanner');
    }

    private static function saveImg($img, $folder): string {
        $imageName = time().'.'.$img->extension(); 
        $img->move(public_path('storage/images/'.$folder), $imageName);
        $path = "storage/images/" .$folder. "/". $imageName;

        return $path;
    }

    private static function deleteImg(int $id, string $variable) {

        $hall = HallModel::find($id);
        $path ='';
        if(isset($hall->$variable)) $path = $hall->$variable;

        $hall->update([$variable => '']);
        
        File::delete(public_path($path));
    }

    public function SEOBlock() {
        return $this->belongsTo(SEOBlockModel::class,'idSEOBlock', 'idSEOBlock');
    }

    public function pictures() {
        return $this->hasMany(PictureModel::class, 'idForeign', 'idHall')->where('slider', 'hall');
    }
    public function sessions() {
        return $this->hasMany(SessionModel::class, 'idHall', 'idHall');
    }
}
