<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VisitStatisticModel extends Model
{
    use HasFactory;

    protected $table = "VisitStatistics";

    protected $primaryKey = 'idVisitStatistic';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'date',
        'device',
    ];

    public $timestamps = false;

}
