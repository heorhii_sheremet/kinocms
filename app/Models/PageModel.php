<?php

namespace App\Models;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PageModel extends Model
{
    use HasFactory;

    protected $table = "Pages";

    protected $primaryKey = 'idPage';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'logo',
        'idSEOBlock',
        'state',
    ];


    public static function store(Request $request){
        
        $page = $request->except(['slides', 'SEOblock']);

        if($request->state !== null) $page['state'] = 1;
        else $page['state'] = 0;

        if(isset($page['logo'])) $page['logo'] = PageModel::saveLogo($page['logo']);

        $SEOBlock = $request->SEOblock;
        $page['idSEOBlock'] = SEOBlockModel::create($SEOBlock)->idSEOBlock;
        
        $page = PageModel::create($page);

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $page->idPage;
            PictureModel::saveSlidePage($slide);
        }

    }

    public static function edit($id) {
        return PageModel::with(['SEOBlock', 'pictures'])->find($id);
    }

    public static function updatePage(Request $request, $id) {

        $pageModel = PageModel::with(['SEOBlock', 'pictures'])->find($id);
        
        $pageRequst = $request->except(['slides', 'SEOblock']);

        if($request->state !== null) $pageRequst['state'] = 1;
        else $pageRequst['state'] = 0;

        if(isset($pageRequst['logo'])) {
            PageModel::deleteLogo($id);
            $pageRequst['logo'] = PageModel::saveLogo($pageRequst['logo']);
        }

        if(null != $request->only('deleteLogo')) PageModel::deleteLogo($id);
        
        $SEOBlock = $request->SEOblock;
        SEOBlockModel::find($pageModel->SEOBlock->idSEOBlock)->update($SEOBlock);
        
        PageModel::find($id)->update($pageRequst);

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $id;
            PictureModel::saveSlidePage($slide);
        }
    }

    public static function destroyPage($id) {

        PageModel::deleteLogo($id);
        
        $page = PageModel::with(['pictures'])->find($id);

        foreach($page->pictures as $picture) PictureModel::deleteSlide($picture->idPicture);

        $page->delete();

    }

    private static function saveLogo($img) {
        return PageModel::saveImg($img, 'logo/page');
    }


    private static function deleteLogo($id) {
        return PageModel::deleteImg($id, 'logo');
    }

    private static function saveImg($img, $folder): string {
        $imageName = time().'.'.$img->extension(); 
        $img->move(public_path('storage/images/'.$folder), $imageName);
        $path = "storage/images/" .$folder. "/". $imageName;

        return $path;
    }

    private static function deleteImg(int $id, string $variable) {

        $page = PageModel::find($id);
        $path ='';
        if(isset($page->$variable)) $path = $page->$variable;

        $page->update([$variable => '']);
        
        File::delete(public_path($path));
    }
    
    public function SEOBlock() {
        return $this->belongsTo(SEOBlockModel::class,'idSEOBlock', 'idSEOBlock');
    }

    public function pictures() {
        return $this->hasMany(PictureModel::class, 'idForeign', 'idPage')->where('slider', 'page');
    }
}
