<?php

namespace App\Models;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ContactModel extends Model
{
    use HasFactory;

    protected $table = "Contacts";

    protected $primaryKey = 'idContact';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'address',
        'coordinates',
        'logo',
        'switch',  
    ];
    
    public $timestamps = false;

    public static function store($contact){
        if(isset($contact['switch'])) $contact['switch'] = '1';
        else $contact['switch'] = '0';
        if(isset($contact['logo'])) $contact['logo'] = ContactModel::saveLogo($contact['logo']);

        if(isset($contact['idContact'])) {

            if(isset($contact['logo'])) ContactModel::deleteLogo($contact['idContact']);

            ContactModel::find($contact['idContact'])->update($contact);
        }

        else ContactModel::create($contact);

    }

    private static function saveLogo($img) {
        $imageName = time().'.'.$img->extension(); 
        $img->move(public_path('storage/images/logo/contacts'), $imageName);
        $path = "storage/images/logo/contacts/". $imageName;

        return $path;
    }


    private static function deleteLogo(int $id) {

        $contact = ContactModel::find($id);
        $path = $contact->logo;

        $contact->update(['logo' => '']);
        
        File::delete(public_path($path));
    }
}
