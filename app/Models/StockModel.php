<?php

namespace App\Models;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


class StockModel extends Model
{
    use HasFactory;

    protected $table = "Stock";

    protected $primaryKey = 'idStock';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'publishedDate',
        'logo',
        'videoLink',
        'idSEOBlock',
        'state'
    ];

    public $timestamps = false;

    public static function store(Request $request){

        $stock = $request->except(['slides', 'SEOblock']);

        if($request->state === null) $stock['state'] = 0;
        else $stock['state'] = 1;

        if(isset($stock['logo'])) $stock['logo'] = StockModel::saveLogo($stock['logo']);

        $SEOBlock = $request->SEOblock;
        $stock['idSEOBlock'] = SEOBlockModel::create($SEOBlock)->idSEOBlock;
        
        $stock = StockModel::create($stock);

        if($request->cinemas !== null) {
            foreach($request->cinemas as $cinema) StockCinemaModel::create([
                'idCinema' => $cinema,
                'idStock' => $stock->idStock
            ]);
        }

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $stock->idStock;
            PictureModel::saveSlideStock($slide);
        }

    }

    public static function edit($id) {
        return StockModel::with(['SEOBlock', 'pictures', 'cinemas'])->find($id);
    }

    public static function updateStock(Request $request, $id) {

        StockModel::deleteRelationsWithCinemas($id);
        if($request->cinemas !== null) {
            foreach($request->cinemas as $cinema) StockCinemaModel::create([
                'idCinema' => $cinema,
                'idStock' => $id
            ]);
        }

        $stockModel = StockModel::with(['SEOBlock', 'pictures'])->find($id);
        
        $stockRequst = $request->except(['slides', 'SEOblock']);

        if($request->state === null) $stockRequst['state'] = 0;
        else $stockRequst['state'] = 1;

        if(isset($stockRequst['logo'])) {
            StockModel::deleteLogo($id);
            $stockRequst['logo'] = StockModel::saveLogo($stockRequst['logo']);
        }

        if(null != $request->only('deleteLogo')) StockModel::deleteLogo($id);
        
        $SEOBlock = $request->SEOblock;
        SEOBlockModel::find($stockModel->SEOBlock->idSEOBlock)->update($SEOBlock);
        
        StockModel::find($id)->update($stockRequst);

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $id;
            PictureModel::saveSlideStock($slide);
        }
    }

    public static function destroyStock($id) {

        StockModel::deleteLogo($id);

        StockModel::deleteRelationsWithCinemas($id);
        
        $stock = StockModel::with(['pictures'])->find($id);

        foreach($stock->pictures as $picture) PictureModel::deleteSlide($picture->idPicture);

        $stock->delete();

    }

    private static function deleteRelationsWithCinemas(int $id) {
        StockCinemaModel::where('idStock', $id)->delete();
    }

    private static function saveLogo($img) {
        return StockModel::saveImg($img, 'logo/stock');
    }


    private static function deleteLogo($id) {
        return StockModel::deleteImg($id, 'logo');
    }

    private static function saveImg($img, $folder): string {
        $imageName = time().'.'.$img->extension(); 
        $img->move(public_path('storage/images/'.$folder), $imageName);
        $path = "storage/images/" .$folder. "/". $imageName;

        return $path;
    }

    private static function deleteImg(int $id, string $variable) {

        $stock = StockModel::find($id);
        $path ='';
        if(isset($stock->$variable)) $path = $stock->$variable;

        $stock->update([$variable => '']);
        
        File::delete(public_path($path));
    }
    
    public function SEOBlock() {
        return $this->belongsTo(SEOBlockModel::class,'idSEOBlock', 'idSEOBlock');
    }

    public function pictures() {
        return $this->hasMany(PictureModel::class, 'idForeign', 'idStock')->where('slider', 'stock');
    }

    public function cinemas() {
        return $this->hasManyThrough(CinemaModel::class ,StockCinemaModel::class, 'idStock', 'idCinema', 'idStock', 'idCinema');
    }

}
