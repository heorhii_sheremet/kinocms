<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File; 

class LetterTemplateModel extends Model
{
    use HasFactory;

    protected $table = 'LetterTemplates';

    protected $primaryKey = 'idLetterTemplate';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'file',
        'path',
    ];

    public $timestamps = false;

    public static function addTemlate($file) {
        $path = LetterTemplateModel::saveTemplate($file);

        $template = LetterTemplateModel::create([
            'file' => $file->getClientOriginalName(),
            'path' => $path,
        ]);

        if(LetterTemplateModel::all()->count() > 5) {
            $templateonDelete = LetterTemplateModel::first();
            File::delete(public_path($templateonDelete->path));
            $templateonDelete->delete();
        }

        return $template;
    }

    private static function saveTemplate($file): string {
            $templateName = time().'.'.$file->extension(); 
            $file->move('storage/templates', $templateName);
            $path = "storage/templates/". $templateName;

            return $path;
    }
}
