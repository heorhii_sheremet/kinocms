<?php

namespace App\Models;

use FFI\Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File; 

class HomePage extends Model
{
    protected $table = 'HomePage';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'phone1',
        'phone2',
        'seoText',
        'idSEOBlock',
        'topRotationalSpeed',
        'newsRotationalSpeed',
        'backgroundBanner'
    ];

    public $timestamps = false;

    public static function saveTopRotationalSpeed($topRotationalSpeed) {
        HomePage::createIfEmpty();
        HomePage::get()->first()->update(['topRotationalSpeed' => $topRotationalSpeed]);
    }

    public static function saveNewsRotationalSpeed($newsRotationalSpeed) {
        HomePage::createIfEmpty();
        HomePage::get()->first()->update(['newsRotationalSpeed' => $newsRotationalSpeed]);
    }
    
    private static function createIfEmpty() {
        if(! HomePage::get()->first()) HomePage::create(['backgroundBanner' => 'storage/images/default-slide.png']);
    }

    public static function saveBanner($banner) {
        HomePage::deleteBanner();
        $imageName = "banner".time().'.'.$banner->extension(); 
        $banner->move(public_path('storage/images/sliders/homePage'), $imageName);
        $backgroundBanner = "storage/images/sliders/homePage/". $imageName;

        HomePage::createIfEmpty();

        HomePage::get()->first()->update(['backgroundBanner' => $backgroundBanner]);
    }

    public static function deleteBanner(){

        HomePage::createIfEmpty();

        File::delete(public_path(HomePage::get()->first()->backgroundBanner));

        HomePage::get()->first()->update(['backgroundBanner' => '']);
        
    }

    public static function store($data) {
        HomePage::createIfEmpty();
        HomePage::get()->first()->update($data);
    }
}
