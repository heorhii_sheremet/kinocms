<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File; 

class PictureModel extends Model
{
    protected $table = 'Pictures';

    protected static $solt = 1;

    protected $primaryKey = 'idPicture';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'url',
        'text',
        'slider',
        'img',
        'idForeign'
    ];

    public $timestamps = false;
    

    public static function saveSlideTop(array $slide) {
        $slide['slider'] = "home_top";
        PictureModel::saveSlide($slide);
    }

    public static function saveSlideNews(array $slide) {
        $slide['slider'] = "home_news";
        PictureModel::saveSlide($slide);
    }

    public static function saveSlideFilm(array $slide) {
        $slide['slider'] = "film";
        PictureModel::saveSlide($slide);
    }

    public static function saveSlideCinema(array $slide) {
        $slide['slider'] = "cinema";
        PictureModel::saveSlide($slide);
    }

    public static function saveSlideHall(array $slide) {
        $slide['slider'] = "hall";
        PictureModel::saveSlide($slide);
    }

    public static function saveSlideNewsPage(array $slide) {
        $slide['slider'] = "news";
        PictureModel::saveSlide($slide);
    }

    public static function saveSlideStock(array $slide) {
        $slide['slider'] = "stock";
        PictureModel::saveSlide($slide);
    }

    public static function saveSlidePage(array $slide) {
        $slide['slider'] = "page";
        PictureModel::saveSlide($slide);
    }
    

    private static function saveSlide(array $slide) { 
        
        $onDelete = $slide['onDelete'] === "false" ? false : true;

        if($onDelete && isset($slide['id']))PictureModel::deleteSlide($slide['id']);
        else {
            if(isset($slide['id'])) PictureModel::updateSlide($slide);
            else PictureModel::createSlide($slide);
        }
    }

    public static function deleteSlide($id) {  
            
        File::delete(public_path(PictureModel::find($id)->img));
        
        PictureModel::find($id)->delete();
            
    }

    private static function updateSlide($slide) {

        if(isset($slide['img'])) {

            $slide['img'] = PictureModel::saveImg($slide['img']);

            File::delete(public_path(PictureModel::find($slide['id'])->img));
        }
        PictureModel::find($slide['id'])->update($slide);
    }

    private static function createSlide($slide) {

        if(isset($slide['img'])) $slide['img'] = PictureModel::saveImg($slide['img']);

        PictureModel::create($slide);
    }

    private static function saveImg($img): string {
            $imageName = time().self::$solt.'.'.$img->extension();
            self::$solt++;
            $img->move(public_path('storage/images/sliders/'), $imageName);
            $path = "storage/images/sliders/". $imageName;

            return $path;
    }
}
