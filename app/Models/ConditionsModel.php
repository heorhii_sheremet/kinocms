<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConditionsModel extends Model
{

    use HasFactory;

    protected $table = "Conditions";

    protected $primaryKey = 'idCondition';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
    ];

    public $timestamps = false;
}
