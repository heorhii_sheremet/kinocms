<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConditionsCinemaModel extends Model
{
    use HasFactory;

    protected $table = "ConditionsCinema";

    protected $primaryKey = 'idConditionsCinema';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'idCondition',
        'idCinema',
    ];

    public $timestamps = false;
}
