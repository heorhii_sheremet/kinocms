<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeatsSessionModel extends Model
{
    use HasFactory;

    protected $table = 'SeatsSessions';

    protected $primaryKey = 'idSeatsSession';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'idSession',
        'idSeat',
        'status',
        'idUser'
    ];

    public $timestamps = false;

    public function user() {
        return $this->hasOne(User::class, 'idUser', 'idUser');
    }
    public function seat() {
        return $this->hasOne(SeatModel::class, 'idSeat', 'idSeat');
    }
}
