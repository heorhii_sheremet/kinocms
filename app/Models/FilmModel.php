<?php

namespace App\Models;

use Illuminate\Support\Facades\File; 
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class FilmModel extends Model
{
    use HasFactory;

    protected $table = "Films";

    protected $primaryKey = 'idFilm';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'mainPicture',
        'trailerLink',
        'type',
        'idSEOBlock',
        'price',
        'status'
    ];

    public $timestamps = false;

    public static function store(Request $request){
              
        $film = $request->except(['slides', 'url', 'title', 'keywords', 'SeoDescription']);
        if(isset($film['logo'])) $film['mainPicture'] = FilmModel::saveImg($film['logo']);

        $SEOBlock = $request->only(['url', 'title', 'keywords', 'SeoDescription']);
        if( isset($SEOBlock['SeoDescription']) ) $SEOBlock['description'] = $SEOBlock['SeoDescription'];
        $film['idSEOBlock'] = SEOBlockModel::create($SEOBlock)->idSEOBlock;
        
        $film = FilmModel::create($film);

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $film->idFilm;
            PictureModel::saveSlideFilm($slide);
        }

    }
    
    public static function edit($id) {
        return FilmModel::with(['SEOBlock', 'pictures', 'sessions'])->find($id);
    }

    public static function updateFilm(Request $request, $id) {

        $filmModel = FilmModel::with(['SEOBlock', 'pictures'])->find($id);
        
        $filmRequst = $request->except(['slides', 'url', 'title', 'keywords', 'SeoDescription']);
        
        if(isset($filmRequst['logo'])) {
            FilmModel::deleteImg($id);
            $filmRequst['mainPicture'] = FilmModel::saveImg($filmRequst['logo']);
        }
        if(null != $request->only('deleteLogo')) FilmModel::deleteImg($id);

        $SEOBlock = $request->only(['url', 'title', 'keywords', 'SeoDescription']);
        if( isset($SEOBlock['SeoDescription']) ) $SEOBlock['description'] = $SEOBlock['SeoDescription'];
        SEOBlockModel::find($filmModel->SEOBlock->idSEOBlock)->update($SEOBlock);
        
        FilmModel::find($id)->update($filmRequst);

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $id;
            PictureModel::saveSlideFilm($slide);
        }
    }

    public static function destroyFilm($id) {

        FilmModel::deleteImg($id);

        $film = FilmModel::with(['pictures', 'sessions'])->find($id);

        foreach($film->pictures as $picture) {
            PictureModel::deleteSlide($picture->idPicture);
        }
        foreach($film->sessions as $sessions) {
            SessionModel::destroySession($sessions->idSession);
        }

        $film->delete();

    }

    private static function saveImg($img): string {
        $imageName = time().'.'.$img->extension(); 
        $img->move(public_path('storage/images/logo/films'), $imageName);
        $path = "storage/images/logo/films/". $imageName;

        return $path;
    }

    private static function deleteImg($id) {

        $film = FilmModel::find($id);
        $path = $film->mainPicture;

        $film->update(['mainPicture' => '']);

        File::delete(public_path($path));
    }

    public function SEOBlock() {
        return $this->belongsTo(SEOBlockModel::class,'idSEOBlock', 'idSEOBlock');
    }

    public function pictures() {
        return $this->hasMany(PictureModel::class, 'idForeign', 'idFilm')->where('slider', 'film');
    }

    public function sessions() {
        return $this->hasMany(SessionModel::class, 'idFilm', 'idFilm')->with('hall');
    }

}
