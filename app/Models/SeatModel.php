<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeatModel extends Model
{
    use HasFactory;

    protected $table = 'Seats';

    protected $primaryKey = 'idSeat';

    public $timestamps = false;
    
}
