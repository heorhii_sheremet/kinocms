<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SEOBlockModel extends Model
{
    use HasFactory;

    protected $table = "SEOBlocks";

    protected $primaryKey = 'idSEOBlock';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'url',
        'title',
        'keywords',
        'description'
    ];

    public $timestamps = false;

    public function owner() {
        return $this->hasOne(FilmModel::class, 'idSEOBlock', 'idSEOBlock');
    }
}
