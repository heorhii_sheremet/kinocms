<?php

namespace App\Models;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


class NewsModel extends Model
{
    use HasFactory;

    protected $table = "News";

    protected $primaryKey = 'idNews';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'publishedDate',
        'logo',
        'videoLink',
        'idSEOBlock',
        'state',
        'idFilm',
    ];

    public $timestamps = false;

    public static function store(Request $request){

        $news = $request->except(['slides', 'SEOblock']);
        if($news['idFilm'] == '-1') $news['idFilm'] = null;

        if($request->state === null) $news['state'] = 0;
        else $news['state'] = 1;

        if(isset($news['logo'])) $news['logo'] = NewsModel::saveLogo($news['logo']);

        $SEOBlock = $request->SEOblock;
        $news['idSEOBlock'] = SEOBlockModel::create($SEOBlock)->idSEOBlock;
        
        $news = NewsModel::create($news);

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $news->idNews;
            PictureModel::saveSlideNewsPage($slide);
        }

    }

    public static function edit($id) {
        return NewsModel::with(['SEOBlock', 'pictures', 'film'])->find($id);
    }

    public static function updateNews(Request $request, $id) {
        
        $newsModel = NewsModel::with(['SEOBlock', 'pictures'])->find($id);
        
        $newsRequst = $request->except(['slides', 'SEOblock']);
        if($newsRequst['idFilm'] == '-1') $newsRequst['idFilm'] = null;

        if($request->state === null) $newsRequst['state'] = 0;
        else $newsRequst['state'] = 1;

        if(isset($newsRequst['logo'])) {
            NewsModel::deleteLogo($id);
            $newsRequst['logo'] = NewsModel::saveLogo($newsRequst['logo']);
        }

        if(null != $request->only('deleteLogo')) NewsModel::deleteLogo($id);
        
        $SEOBlock = $request->SEOblock;
        SEOBlockModel::find($newsModel->SEOBlock->idSEOBlock)->update($SEOBlock);
        
        NewsModel::find($id)->update($newsRequst);

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $id;
            PictureModel::saveSlideNewsPage($slide);
        }
    }

    public static function destroyNews($id) {

        NewsModel::deleteLogo($id);
        
        $news = NewsModel::with(['pictures'])->find($id);

        foreach($news->pictures as $picture) PictureModel::deleteSlide($picture->idPicture);

        $news->delete();

    }

    private static function saveLogo($img) {
        return NewsModel::saveImg($img, 'logo/news');
    }


    private static function deleteLogo($id) {
        return NewsModel::deleteImg($id, 'logo');
    }

    private static function saveImg($img, $folder): string {
        $imageName = time().'.'.$img->extension(); 
        $img->move(public_path('storage/images/'.$folder), $imageName);
        $path = "storage/images/" .$folder. "/". $imageName;

        return $path;
    }

    private static function deleteImg(int $id, string $variable) {

        $news = NewsModel::find($id);
        $path ='';
        if(isset($news->$variable)) $path = $news->$variable;

        $news->update([$variable => '']);
        
        File::delete(public_path($path));
    }
    
    public function SEOBlock() {
        return $this->belongsTo(SEOBlockModel::class,'idSEOBlock', 'idSEOBlock');
    }

    public function pictures() {
        return $this->hasMany(PictureModel::class, 'idForeign', 'idNews')->where('slider', 'news');
    }

    public function film() {
        return $this->hasOne(FilmModel::class, 'idFilm', 'idFilm');
    }

}
