<?php

namespace App\Models;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CinemaModel extends Model
{
    use HasFactory;

    protected $table = "Cinemas";

    protected $primaryKey = 'idCinema';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'logo',
        'idSEOBlock',
        'banner'  
    ];

    public $timestamps = false;

    public static function store(Request $request){
        
        $cinema = $request->except(['slides', 'SEOblock']);
        if(isset($cinema['logo'])) $cinema['logo'] = CinemaModel::saveLogo($cinema['logo']);
        if(isset($cinema['banner'])) $cinema['banner'] = CinemaModel::saveBanner($cinema['banner']);

        if($request->SEOblock !== null) {
            $SEOBlock = $request->SEOblock;
            $cinema['idSEOBlock'] = SEOBlockModel::create($SEOBlock)->idSEOBlock;
        }
        
        $cinema = CinemaModel::create($cinema);

        if($request->conditions !== null) {
            foreach($request->conditions as $condition) ConditionsCinemaModel::create([
                'idCinema' => $cinema->idCinema,
                'idCondition' => $condition,
            ]);
        }

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $cinema->idCinema;
            PictureModel::saveSlideCinema($slide);
        }

    }

    public static function edit($id) {
        return CinemaModel::with(['SEOBlock', 'pictures', 'halls', 'conditions'])->find($id);
    }

    public static function updateCinema(Request $request, $id) {

        CinemaModel::deleteRelationsWithConditions($id);
        if($request->conditions !== null) {

            foreach($request->conditions as $condition) ConditionsCinemaModel::create([
                'idCinema' => $id,
                'idCondition' => $condition,
            ]);
        }

        $cinemaModel = CinemaModel::with(['SEOBlock', 'pictures'])->find($id);
        
        $cinemaRequst = $request->except(['slides', 'SEOblock']);

        if(isset($cinemaRequst['logo'])) {
            CinemaModel::deleteLogo($id);
            $cinemaRequst['logo'] = CinemaModel::saveLogo($cinemaRequst['logo']);
        }
        if(isset($cinemaRequst['banner'])) {
            CinemaModel::deleteBanner($id);
            $cinemaRequst['banner'] = CinemaModel::saveBanner($cinemaRequst['banner']);
        } 

        if(null != $request->only('deleteLogo')) CinemaModel::deleteLogo($id);
        if(null != $request->only('deleteBanner')) CinemaModel::deleteBanner($id);

        if($request->SEOblock !== null) {
            $SEOBlock = $request->SEOblock;
            SEOBlockModel::find($cinemaModel->SEOBlock->idSEOBlock)->update($SEOBlock);
        }
        
        CinemaModel::find($id)->update($cinemaRequst);

        $slides = $request->slides;
        
        if(is_array($slides)) foreach( $slides as $slide) {  
            $slide['idForeign'] = $id;
            PictureModel::saveSlideCinema($slide);
        }
    }

    public static function destroyCinema($id) {

        CinemaModel::deleteLogo($id);
        CinemaModel::deleteBanner($id);
        CinemaModel::deleteRelationsWithConditions($id);
        
        $cinema = CinemaModel::with(['pictures', 'halls'])->find($id);

        foreach($cinema->pictures as $picture) PictureModel::deleteSlide($picture->idPicture);
        foreach($cinema->halls as $hall) HallModel::destroyHall($hall->idHall);

        $cinema->delete();

    }

    private static function deleteRelationsWithConditions(int $id) {
        ConditionsCinemaModel::where('idCinema', $id)->delete();
    }

    private static function saveLogo($img) {
        return CinemaModel::saveImg($img, 'logo/cinemas');
    }

    private static function saveBanner($img) {
        return CinemaModel::saveImg($img, 'banner/cinemas');
    }

    private static function deleteLogo($id) {
        return CinemaModel::deleteImg($id, 'logo');
    }

    private static function deleteBanner($id) {
        return CinemaModel::deleteImg($id, 'banner');
    }

    private static function saveImg($img, $folder): string {
        $imageName = time().'.'.$img->extension(); 
        $img->move(public_path('storage/images/'.$folder), $imageName);
        $path = "storage/images/" .$folder. "/". $imageName;

        return $path;
    }

    private static function deleteImg(int $id, string $variable) {

        $hall = CinemaModel::find($id);
        $path ='';
        if(isset($hall->$variable)) $path = $hall->$variable;

        $hall->update([$variable => '']);
        
        File::delete(public_path($path));
    }

   public static function getTodaySessionById(int $id){

        $cinema = CinemaModel::with(['halls'])->find($id);
        $hallsId = [];
        foreach($cinema->halls as $hall) $hallsId [] = $hall->idHall;
        //dd($hallsId);
        $sessionsToday = SessionModel::where('date', 'like', date('Y-m-d') .'%')->whereIn('idHall', $hallsId)->get();
       
        return $sessionsToday;
    }
    
    public function SEOBlock() {
        return $this->belongsTo(SEOBlockModel::class,'idSEOBlock', 'idSEOBlock');
    }

    public function pictures() {
        return $this->hasMany(PictureModel::class, 'idForeign', 'idCinema')->where('slider', 'cinema');
    }

    public function halls() {
        return $this->hasMany(HallModel::class, 'idCinema', 'idCinema')->with('sessions');
    }

    public function conditions() {
        return $this->hasManyThrough(ConditionsModel::class ,ConditionsCinemaModel::class, 'idCinema', 'idCondition', 'idCinema', 'idCondition');
    }
}
