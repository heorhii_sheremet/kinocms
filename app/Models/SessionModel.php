<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class SessionModel extends Model
{
    use HasFactory;

    protected $table = "Sessions";

    protected $primaryKey = 'idSession';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'idFilm',
        'idHall',
        'date',
    ];

    public $timestamps = false;

    public static function store(Request $request){
        
        $session = $request->all();
        $session = SessionModel::create($session);

        return $session;

    }

    public static function edit($id) {
        return SessionModel::with(['cinema', 'hall'])->find($id);
    }

    public static function updateSession(Request $request, $id) {

        $session = $request->all();
        
        SessionModel::find($id)->update($session);

    }

    public static function destroySession($id) {

        $session = SessionModel::find($id);
        $idFilm = $session->idFilm;

        $session->delete();

        return $idFilm;

    }

    public static function groupByDate() {
       $sessions = SessionModel::groupBy('date')->get();
       dd($sessions);
    }

    public function cinema() {
        return $this->hasOneThrough(CinemaModel::class, HallModel::class,'idHall', 'idCinema', 'idHall', 'idCinema');
    }

    public function hall() {
        return $this->hasOne(HallModel::class,'idHall', 'idHall');
    }

    public function film() {
        return $this->hasOne(FilmModel::class,'idFilm', 'idFilm');
    }

    public function seatsSession() {
        return $this->hasMany(SeatsSessionModel::class, 'idSession', 'idSession');
    }

}
