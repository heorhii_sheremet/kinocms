<?php

namespace App\Http\Controllers;

use App\Models\CinemaModel;
use App\Models\FilmModel;
use Illuminate\Http\Request;

class FilmCardController extends Controller
{
    public static function index($id) {
        $film = FilmModel::with(['SEOBlock', 'pictures', 'sessions'])->find($id);
        $cinemas = CinemaModel::all();
        return view('film-card', ['film' => $film, 'cinemas' => $cinemas]);
    }
}
