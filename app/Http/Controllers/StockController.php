<?php

namespace App\Http\Controllers;

use App\Models\StockModel;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public static function index() {
        $stock = StockModel::with(['cinemas'])->get();

        return view('stock', ['stock' => $stock]);
    }

    public static function show($id) {

        $stock = StockModel::with(['SEOBlock', 'pictures', 'cinemas'])->find($id);

        return view('stock-card', ['stock' => $stock]);

    }
}
