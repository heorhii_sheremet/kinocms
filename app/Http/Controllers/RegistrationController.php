<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\VisitStatisticModel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Facades\Agent;

class RegistrationController extends Controller
{
    public static function login() {
        return view('login');
    }


    public static function auth(Request $request) {
    
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {

            $request->session()->regenerate();

            if(auth()->id()) VisitStatisticModel::create([
                'date' => date('Y:m:d'),
                'device' => Agent::isDesktop() ? 'desktop' : 'mobile'
            ]);

            if(auth()->id() === 1) return redirect('/admin');
            return redirect()->intended('/');
        }

        return back()->withErrors([
            'error_auth' => 'Login or password is incorrect',
        ]);
    }


    public static function registration() {
        return view('registration');
    }


    public static function userSave(Request $request) {

        $user = $request->validate([
            'nickName' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        $user['password'] = Hash::make($user['password']);
        try {
            User::create($user);
        } 
        catch(QueryException $e){
            $error = 'Error';
            if($e->getCode() === "23000") $error = 'Email already registered';
            return back()->withErrors([
                'email_alredy_is' => $error,
            ]);
            
        }

        return redirect()->route('login');
       
    }


    public static function account() {
        return view('account');
    }


    public static function accountSave(Request $request) {
        if($request->email) return redirect()->route('account');
        User::updateUser($request, auth()->id());
        return redirect()->route('account');
    }

    public function logout(Request $request) {

        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
