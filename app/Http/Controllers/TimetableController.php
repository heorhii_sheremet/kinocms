<?php

namespace App\Http\Controllers;

use App\Models\CinemaModel;
use App\Models\FilmModel;
use App\Models\SeatModel;
use App\Models\SeatsSessionModel;
use App\Models\SessionModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TimetableController extends Controller
{
    public static function index(Request $request) {

        $sessionsByDatetime = TimetableController::filter($request);

        $sessionsByDate = TimetableController::sessionGroupByFromDatetimeToData($sessionsByDatetime);
        $films = FilmModel::all();
        $cinemas = CinemaModel::all();
        return view('timetable', ['sessionsByDate' => $sessionsByDate, 'films' => $films, 'cinemas' => $cinemas, 'filters' => $request->all()]);
    }

    public static function bookingTicket($id) {

        $session = SessionModel::with(['hall', 'cinema', 'film', 'seatsSession'])->find($id);

        $seats = SeatModel::all()->groupBy('row');

        return view('booking', ['session' => $session, 'seats' => $seats]);
    }

    public static function saveBooking(Request $request) {
        if(isset($request->tickets)) {
            foreach($request->tickets as $ticket) {
                
                SeatsSessionModel::create([
                        'idSession' => $request->idSession,
                        'idSeat' => $ticket,
                        'idUser' => auth()->id(),
                        'status' => $request->status
                ]);
                
            }
        }

        return redirect()->route('booking', ['idSession' => $request->idSession]);
    } 

    private static function sessionGroupByFromDatetimeToData( $sessionsByDatetime) {
        $sessionsByDate = collect([]); // колекция для сортировки только по дате
        foreach($sessionsByDatetime as $datatime => $el) {
            $data = (string) date('Y-m-d', strtotime($datatime)); // извлекаем дату

            if(isset($sessionsByDate[$data])) $sessionsByDate[$data]->push($el); // если дата есть, добавляем записи к этой дате
            
            else {//если даты нет
                $sessionsByDate->put($data, collect()); // создаем елемент колеции, где ключ это наша дата
                $sessionsByDate[$data]->push($el); // добавляем в елемент колекцци(где ключ это наша дата) записи
            }

            $sessionsByDate[$data] = $sessionsByDate[$data]->flatten(); // убираем вложеность колекций в текущем елементе

        }

        return $sessionsByDate;
    }

    private static function filter(Request $request) {
        if($request->date !== null) {// if is date 

            $sessionsByDatetime = SessionModel::join('Halls', 'Sessions.idHall', '=', 'Halls.idHall')
            ->join('Films', 'Sessions.idFilm', '=', 'Films.idFilm')
            ->select('Sessions.*', 'Halls.idCinema', 'Films.type')//join idCinema, type fields
            ->where('date', 'like', $request->date .'%')->get();// filter by date 

        } else {//if is not date

            $sessionsByDatetime = SessionModel::join('Halls', 'Sessions.idHall', '=', 'Halls.idHall')
            ->join('Films', 'Sessions.idFilm', '=', 'Films.idFilm')
            ->select('Sessions.*', 'Halls.idCinema', 'Films.type')->get(); // only join idCinema, type fields

        }

        if($request->film !== null) $sessionsByDatetime = $sessionsByDatetime->where('idFilm', $request->film);
        if($request->hall !== null) $sessionsByDatetime = $sessionsByDatetime->where('idHall', $request->hall);
        if($request->cinema !== null) $sessionsByDatetime = $sessionsByDatetime->where('idCinema', $request->cinema);
        if($request->type !== null) $sessionsByDatetime = $sessionsByDatetime->where('type', $request->type);
    
        return $sessionsByDatetime->groupBy('date');   
    }
}
