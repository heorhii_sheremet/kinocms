<?php

namespace App\Http\Controllers;

use App\Models\CinemaModel;
use App\Models\HallModel;
use App\Models\SeatModel;
use Illuminate\Http\Request;

class CinemaController extends Controller
{
    public static function index() {
        $cinemas = CinemaModel::get(['idCinema', 'name', 'logo']);
        return view('cinemas', ['cinemas' => $cinemas]);
    }

    public static function show($id) {
        $cinema = CinemaModel::with(['halls'])->find($id);

        $sessions =CinemaModel::getTodaySessionById($id);

        return view('cinema-card', ['cinema' => $cinema, 'sessions' => $sessions]);
    }

    public static function showHall($id) {
        
        $hall = HallModel::with('pictures')->find($id);
        $seats = SeatModel::all()->groupBy('row');
        $sessions = HallModel::getTodaySessionById($id);

        return view('hall-card', [
            'hall' => $hall,
            'seats' => $seats,
            'sessions' =>$sessions
            ]);
    }
}
