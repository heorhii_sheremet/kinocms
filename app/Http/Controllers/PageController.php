<?php

namespace App\Http\Controllers;

use App\Models\ContactModel;
use App\Models\PageModel;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public static function index($id) {
        $page = PageModel::with(['pictures'])->find($id);
        return view('page', ['page' => $page]);
    }

    public static function contacts() {
        $contacts = ContactModel::all();
        return view('contacts', ['contacts' => $contacts]);
    }

    public static function mobileApp() {
        return view('mobile-app');
    }
}
