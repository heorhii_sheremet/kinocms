<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\SessionModel;
use App\Models\User;
use App\Models\VisitStatisticModel;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use League\CommonMark\Extension\CommonMark\Node\Block\IndentedCode;

class IndexController extends Controller
{
    public function __invoke()
    {   
        $sessions = SessionModel::all()->groupBy('date');
        $sessions = IndexController::sessionGroupByFromDatetimeToData($sessions);
        $sessions = IndexController::countByDate($sessions);

        $desktopVisitorsbyDate = VisitStatisticModel::select('date', VisitStatisticModel::raw('count(*) as count'))
            ->where('device', 'desktop')
            ->groupBy('date')
            ->get();
        
        $mobileVisitorsbyDate = VisitStatisticModel::select('date', VisitStatisticModel::raw('count(*) as count'))
            ->where('device', 'mobile')
            ->groupBy('date')
            ->get();
       
        $users = User::all();
        
        return view('admin.statistic.statistic', [
            'sessions' => $sessions,
            'desktopVisitorsbyDate' => $desktopVisitorsbyDate,
            'mobileVisitorsbyDate' => $mobileVisitorsbyDate,
            'users' => $users
        ]);
    }

    private static function sessionGroupByFromDatetimeToData( $sessionsByDatetime) {
        $sessionsByDate = collect([]); // колекция для сортировки только по дате
        foreach($sessionsByDatetime as $datatime => $el) {
            $data = (string) date('Y-m-d', strtotime($datatime)); // извлекаем дату

            if(isset($sessionsByDate[$data])) $sessionsByDate[$data]->push($el); // если дата есть, добавляем записи к этой дате
            
            else {//если даты нет
                $sessionsByDate->put($data, collect()); // создаем елемент колеции, где ключ это наша дата
                $sessionsByDate[$data]->push($el); // добавляем в елемент колекцци(где ключ это наша дата) записи
            }

            $sessionsByDate[$data] = $sessionsByDate[$data]->flatten(); // убираем вложеность колекций в текущем елементе

        }

        return $sessionsByDate;
    }

    private static function countByDate($sessions) {
        $sumSessionByDate = [];
        foreach($sessions as $key => $session) $sumSessionByDate [$key] = $session->count();
        
        return $sumSessionByDate;
    }
}
