<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PageModel;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = PageModel::get(['idPage', 'created_at', 'name']);

        return view('admin.pages.index', ['pages' => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.page');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        PageModel::store($request);
        return redirect()->route("pages.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PageModel  $pageModel
     * @return \Illuminate\Http\Response
     */
    public function show(PageModel $pageModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PageModel  $pageModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = PageModel::edit($id);
        return view('admin.pages.page', ['page' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PageModel  $pageModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        PageModel::updatePage($request, $id);
        return redirect()->route("pages.edit", ['page' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PageModel  $pageModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PageModel::destroyPage($id);

        return redirect()->route("pages.index");
    }
}
