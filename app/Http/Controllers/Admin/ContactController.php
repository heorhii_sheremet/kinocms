<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContactModel;
use App\Models\SEOBlockModel;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = ContactModel::all();
        $SEOBlock = SEOBlockModel::find(1); // запись выделена под страницу контактов idSEOBlock=1

        return view('admin.pages.contacts', ['contacts' => $contacts, 'SEOBlock' => $SEOBlock]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->contacts);
        if(is_array($request->contacts)) foreach($request->contacts as $contact) ContactModel::store($contact);
        SEOBlockModel::find(1)->update($request->SEOblock);

        return redirect()->route("contacts.index");
    }

}
