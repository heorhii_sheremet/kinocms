<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CinemaModel;
use App\Models\StockModel;
use Illuminate\Http\Request;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stock = StockModel::get(['idStock', 'logo', 'name']);

        return view('admin.stock.index', ['stock' => $stock]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cinemas = CinemaModel::all();
        return view('admin.stock.stock', ['cinemas' => $cinemas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        StockModel::store($request);
        return redirect()->route("stock.index");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StockModel  $stockModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stock = StockModel::edit($id);
        $cinemas = CinemaModel::all();
        return view('admin.stock.stock', ['stock' => $stock, 'cinemas' => $cinemas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StockModel  $stockModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        StockModel::updateStock($request, $id);
        return redirect()->route("stock.edit", ['stock' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StockModel  $stockModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StockModel::destroyStock($id);

        return redirect()->route("stock.index");
    }
}
