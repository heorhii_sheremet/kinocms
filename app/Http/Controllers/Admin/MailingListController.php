<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailJob;
use App\Mail\Spam;
use App\Models\JobModel;
use App\Models\LetterTemplateModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailingListController extends Controller
{
    public static function index() {
    
        return view('admin.mailing-list.index',[
            'templates' => LetterTemplateModel::all(),
            'users'     => User::all(),
        ]);
    }

    public static function send(Request $request) {
        $r = $request->all();
        $temlate = '';
        if( $request->temlateType == "downloadTemplate" && isset($request->downloadedTemplate) ) {
            $temlate = LetterTemplateModel::addTemlate($request->downloadedTemplate);
            //dd($temlate);
        }
        elseif( $request->temlateType == "chooseTemplate" && isset($request->template) ) {
            $temlate = LetterTemplateModel::find( $request->template );
            //($temlate);
        }
        if($temlate === '' || ( !isset($request->sendEvryone) && !isset($request->users) ) ) return ['error' => 'Check selected data'];
        
        if( isset($request->sendEvryone) ){
            foreach(User::all() as $index => $user) dispatch(new SendEmailJob(['user' => $user->email, 'template' => $temlate]))->delay(now()->addSecond(1));
        }
        elseif( isset($request->users) ) {
            foreach($request->users as $index => $idUser) dispatch(new SendEmailJob([
                'user' => User::find($idUser)->email,
                'template' => $temlate
                ]))
                ->delay(now()->addSecond(1));
        }
        

        return ['success' => 'Messages sent'];

        
    }

    public static function getQueueCount() {
        return ['left' => JobModel::count()];
    }
}
