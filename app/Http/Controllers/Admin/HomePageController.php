<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HomePage;
use App\Models\SEOBlockModel;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = HomePage::get()->first();
        $SEOBlock = SEOBlockModel::find(2); // запись выделена под страницу контактов idSEOBlock=2

        return view('admin.pages.home-page', ['data' => $data, 'SEOBlock' => $SEOBlock]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        HomePage::store($request->except('SEOblock'));
        SEOBlockModel::find(2)->update($request->SEOblock);

        return redirect()->route("home-page.index");
    }

}
