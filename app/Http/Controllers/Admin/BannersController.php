<?php

namespace App\Http\Controllers\Admin;

use App\Models\PictureModel;
use App\Http\Controllers\Controller;
use App\Models\HomePage;
use App\Services\Slide\Slide;
use Jenssegers\Agent\Facades\Agent;

use Illuminate\Http\Request;

class BannersController extends Controller
{   

    public function index()
    {   
        $slidesTop = PictureModel::all()->where('slider', '=', 'home_top');
        $slidesNews = PictureModel::all()->where('slider', '=', 'home_news');
        if(HomePage::get()->first()) $settingHomePage = HomePage::get()->first();
        else $settingHomePage = null;
        return view('admin.banners', ['slidesTop' => $slidesTop, 'slidesNews' => $slidesNews, 'settingHomePage' => $settingHomePage]);
    }

    public function saveSliderTop(Request $request)
    { 
        $slides = $request->slides;

        if(is_array($slides)) foreach( $slides as $slide) {  
            PictureModel::saveSlideTop($slide);
        }

        HomePage::saveTopRotationalSpeed($request->rotationalSpeed);

        return redirect()->route("banners");
        
    } 

    public function saveSliderNews(Request $request)
    { 
        $slides = $request->slides;

        if(is_array($slides)) foreach( $slides as $slide) {  
            PictureModel::saveSlideNews($slide);
        }

        HomePage::saveNewsRotationalSpeed($request->rotationalSpeedNews);

        return redirect()->route("banners");
        
    }

    public function saveBanner(Request $request) {

        HomePage::saveBanner($request->banner);

        return redirect()->route("banners");
        
    }

    public function deleteBanner() {

        HomePage::deleteBanner();

        return redirect()->route("banners");
    }


}

