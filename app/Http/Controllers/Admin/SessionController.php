<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CinemaModel;
use App\Models\FilmModel;
use App\Models\SeatsSessionModel;
use App\Models\SessionModel;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idFilm)
    {
        $cinemas = CinemaModel::with('halls')->get();
        $film = FilmModel::find($idFilm);
        return view('admin.sessions.session', ['film' => $film, 'cinemas' => $cinemas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session = SessionModel::store($request);
        $film = FilmModel::edit($session->idFilm);
        return redirect()->route("films.edit", ['film' => $film]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SessionModel  $sessionModel
     * @return \Illuminate\Http\Response
     */
    public function show(SessionModel $sessionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SessionModel  $sessionModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $session = SessionModel::edit($id);
        $cinemas = CinemaModel::with('halls')->get();
        $film = FilmModel::find($session->idFilm);
        $seatsSession = SeatsSessionModel::with('user', 'seat')->where('idSession', $id)->get();

        return view('admin.sessions.session', ['film' => $film, 'cinemas' => $cinemas, 'session' => $session, 'seatsSession' => $seatsSession]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SessionModel  $sessionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        SessionModel::updateSession($request, $id);
        return redirect()->route("sessions.edit", ['session' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SessionModel  $sessionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idFilm = SessionModel::destroySession($id);

        return redirect()->route("films.edit", ['film' => $idFilm]);
    }
}
