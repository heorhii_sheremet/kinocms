<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CinemaModel;
use App\Models\HallModel;
use Illuminate\Http\Request;

class HallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idCinema)
    {
        return view('admin.halls.hall', ['idCinema' => $idCinema]);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $hall = HallModel::store($request);
        $cinema = CinemaModel::edit($hall->idCinema);
        return redirect()->route("cinemas.edit", ['cinema' => $cinema]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HallModel  $hallModel
     * @return \Illuminate\Http\Response
     */
    public function show(HallModel $hallModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HallModel  $hallModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hall = HallModel::edit($id);
        return view('admin.halls.hall', ['idCinema' => $hall->idCinema, 'hall' => $hall]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HallModel  $hallModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HallModel::updateHall($request, $id);
        return redirect()->route("halls.edit", ['hall' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HallModel  $hallModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idCinema = HallModel::destroyHall($id);

        $cinema = CinemaModel::edit($idCinema);

        return redirect()->route("cinemas.edit", ['cinema' => $cinema]);
    }
}
