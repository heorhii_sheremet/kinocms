<?php

namespace App\Http\Controllers\Admin;

use App\Models\PictureModel;
use App\Http\Controllers\Controller;
use App\Models\ConditionsModel;
use App\Models\FilmModel;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = FilmModel::get(['idFilm', 'mainPicture', 'name', 'status']);

        return view('admin.films.index', ['films' => $films]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.films.film');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        FilmModel::store($request);
        return redirect()->route("films.index");
    }

    public function show($id) {
        
        $data = FilmModel::show($id);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FilmModel  $filmModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = FilmModel::edit($id);
        return view('admin.films.film', ['film' => $film]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        FilmModel::updateFilm($request, $id);
        
        return redirect()->route("films.edit", ['film' => $id]);
    }

    public function destroy($id)
    {

        FilmModel::destroyFilm($id);

        return redirect()->route("films.index");
    }

    
}
