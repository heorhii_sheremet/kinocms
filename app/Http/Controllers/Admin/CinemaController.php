<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CinemaModel;
use App\Models\ConditionsModel;
use Illuminate\Http\Request;

class CinemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cinemas = CinemaModel::get(['idCinema', 'logo', 'name']);
        $conditions = ConditionsModel::get(['idCondition', 'name']);

        return view('admin.cinema.index', ['cinemas' => $cinemas, 'conditions' => $conditions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cinema.cinema');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        CinemaModel::store($request);
        return redirect()->route("cinemas.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CinemaModel  $cinemaModel
     * @return \Illuminate\Http\Response
     */
    public function show(CinemaModel $cinemaModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CinemaModel  $cinemaModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cinema = CinemaModel::edit($id);
        $conditions = ConditionsModel::get(['idCondition', 'name']);
        return view('admin.cinema.cinema', ['cinema' => $cinema, 'conditions' => $conditions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CinemaModel  $cinemaModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        CinemaModel::updateCinema($request, $id);
        return redirect()->route("cinemas.edit", ['cinema' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CinemaModel  $cinemaModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CinemaModel::destroyCinema($id);

        return redirect()->route("cinemas.index");
    }
}
