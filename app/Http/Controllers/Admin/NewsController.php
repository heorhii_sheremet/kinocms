<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FilmModel;
use App\Models\NewsModel;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = NewsModel::get(['idNews', 'logo', 'name']);

        return view('admin.news.index', ['news' => $news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $films = FilmModel::all();
        return view('admin.news.news', ['films' => $films]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        NewsModel::store($request);
        return redirect()->route("news.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NewsModel  $newsModel
     * @return \Illuminate\Http\Response
     */
    public function show(NewsModel $newsModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NewsModel  $newsModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = NewsModel::edit($id);
        $films = FilmModel::all();
        return view('admin.news.news', ['news' => $news, 'films' => $films]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NewsModel  $newsModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        NewsModel::updateNews($request, $id);
        return redirect()->route("news.edit", ['news' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NewsModel  $newsModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NewsModel::destroyNews($id);

        return redirect()->route("news.index");
    }
}
