<?php

namespace App\Http\Controllers;

use App\Models\NewsModel;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public static function index() {
        $news = NewsModel::with(['film'])->get();

        return view('news', ['news' => $news]);
    }

    public static function show($id) {

        $news = NewsModel::with(['SEOBlock', 'pictures', 'film'])->find($id);

        return view('news-card', ['news' => $news]);

    }
}
