<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class LocalizationController extends Controller
{
    public static function index(Request $request ,$lang) {
        session(['lang' => $lang]);
        return redirect(url()->previous());
    }

    
}
