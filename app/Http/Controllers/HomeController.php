<?php

namespace App\Http\Controllers;

use App\Models\FilmModel;
use App\Models\HomePage;
use App\Models\PictureModel;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public static function index() {
        $films = FilmModel::get(['idFilm', 'mainPicture', 'name', 'status']);
        $pictures = PictureModel::where('idForeign', '-1')->get();

        return view('home', ['films' => $films, 'pictures' => $pictures]);
    }
}
