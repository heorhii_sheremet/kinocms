<?php

namespace App\Http\Controllers;

use App\Models\FilmModel;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;

class PosterController extends Controller
{
    public static function index(Request $request) {
        $films = FilmModel::get(['idFilm', 'mainPicture', 'name', 'status']);
        return view('poster', ['films' => $films, 'soon' => $request->soon]);
    }
}
