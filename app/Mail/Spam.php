<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use UnexpectedValueException;

class Spam extends Mailable
{
    use Queueable, SerializesModels;

    private $template;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($template)
    {
        $this->template = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $t = $this->template;
        $t = $this->template->path;
        $t = (public_path($this->template->path));
        try{   
            $content = file_get_contents($t);
        }
        catch(UnexpectedValueException $e) {
            dd($e);
        }
        return $this->from('kinocms@ukr.net', 'KinoCMS')
        ->view('emails.template')
        ->with(['text' => $content]);
    }
}
