@extends('layout')
@section('content')
<div class="row">
    <div class="row ms-2 mb-3">
        <iframe width="560" height="500" src="{{$stock->videoLink}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="col-9">
        <div class="container-fluent bg-secondary ms-1 ps-3 pb-4">
            <span class="display-6 text-white">{{$stock->name}}</span><br><br>
            @foreach($stock->cinemas as $cinema)
                <span class="text-white bg-info mt-5 ms-1 rounded-pill p-2">{{$cinema->name}}</span>
            @endforeach
            <div class="row mt-5">
                <div class="col-6">
                    <img src="{{asset($stock->logo)}}" alt="{{__('Image')}}" style="width: 99%; max-height: 600px;">
                </div>
                <div class="col-6 text-break pe-4">
                    <span>{{$stock->description}}</span>
                </div>
            </div>
            <div class="row mt-2 pe-2">
                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel" style=" width: 100%; height: 700px; overflow: hidden;">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{isset($stock->logo) ? asset($stock->logo) : asset(config("app.defaultPicture"))}}" class="d-block w-100" alt="...">
                        </div>
                        @foreach($stock->pictures as $picture)
                        <div class="carousel-item">
                            <img src="{{isset($picture->img) ? asset($picture->img) : asset(config("app.defaultPicture"))}}" class="d-block w-100" alt="...">
                        </div>
                        @endforeach
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-3 bg-lght pt-5">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id facilis sequi incidunt a molestiae, iusto minima. Autem quod odio nesciunt, fugit dolores alias vitae at obcaecati, eum sed voluptate ex!
    </div>

</div>
@endsection