@extends('layout')
@section('content')
<div class="row">
    <iframe width="560" height="500" src="{{$film->trailerLink}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<div class="container mt-5">
    <div class="row bg-light pt-3">
       <div class="col-auto">
           <div class="row">
                <p class="col-auto pt-2">
                    {{__('Cinema')}}
                </p>
                <div class="col-auto">
                    <select class="form-select mb-3" id='cinema'>
                        <option value="-1">{{__('All')}}</option>
                        @foreach($cinemas as $cinema)
                            <option value="{{$cinema->idCinema}}">{{$cinema->name}}</option>
                        @endforeach
                    </select>
                </div>
           </div>
        </div>
        <div class="col-auto">
            <div class="row">
                <p class="col-auto pt-2">
                    {{__('Date')}}
                </p>
                <div class="col-auto">
                    <input type="date" class="form-control" id='date'>
                </div>
            </div>
        </div>
    </div>

    <div class="row bg-light p-3 mt-3" id='sessionWrapper'>
        @foreach($film->sessions as $session)
            <div class="card me-3" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">{{$cinemas->find($session->hall->idCinema)->name}}</h5>
                  <p class="card-text">{{__('Date')}}: {{date('D d.m', strtotime($session->date))}}</p>
                  <p class="card-text">{{__('Time')}}: {{date('H:i', strtotime($session->date))}}</p>
                  <p class="card-text">{{__('Hall')}} {{$session->hall->number}}</p>
                </div>
            </div>
        @endforeach
    </div>

    <div class="row bg-light p-3 mt-3">
        <div class="col-auto">
            <img src="{{isset($film->mainPicture) ? asset($film->mainPicture) : asset(config("app.defaultPicture"))}}" width="600" alt="Logo">
        </div>
        <div class="col text-center">
            <a href="#" class="btn btn-success btn-lg">{{__('Buy ticket')}}</a>
            <p class="display-6">{{$film->name}}</p>
            <p>{{__('Price')}}: {{$film->price}}</p>
            <p>{{__('Type')}}: {{$film->type}}</p>
            <p>{{$film->description}}</p>
        </div>
    </div>
    <div class="row bg-light p-3 mt-3">
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{isset($film->mainPicture) ? asset($film->mainPicture) : asset(config("app.defaultPicture"))}}" class="d-block w-100" alt="...">
                </div>
                @foreach($film->pictures as $picture)
                <div class="carousel-item">
                    <img src="{{isset($picture->img) ? asset($picture->img) : asset(config("app.defaultPicture"))}}" class="d-block w-100" alt="...">
                </div>
                @endforeach
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
    <div class="row bg-light p-3 mt-3">
        <div class="col-12 text-center">
            Ad
        </div>
    </div>
</div>
<script>
    $('#cinema, #date').change( function() {
        $('#sessionWrapper').empty()

        idCinema = $('#cinema').val()
        date = $('#date').val()

        @foreach($film->sessions as $session)
           if(idCinema === "{{$session->hall->idCinema}}" || idCinema === '-1') {
                if(date === "{{date('Y-m-d', strtotime($session->date))}}" || date === '') {

                    $('#sessionWrapper').append(
                        '<div class="card me-3" style="width: 18rem;">'+
                        '<div class="card-body">'+
                        '<h5 class="card-title">{{$cinemas->find($session->hall->idCinema)->name}}</h5>'+
                        '<p class="card-text">{{__('Date')}}: {{date('D d.m', strtotime($session->date))}}</p>'+
                        '<p class="card-text">{{__('Time')}}: {{date('H:i', strtotime($session->date))}}</p>'+
                        '<p class="card-text">{{__('Hall')}} {{$session->hall->number}}</p>'+
                        '</div>'+
                    '</div>'
                    )

                }
            }
        @endforeach
    })
</script>
@endsection