@extends('layout')
@section('content')
<div class="text-center">
    <img src="{{asset($cinema->banner)}}" alt="{{__('Image')}}" style="width: 98%; max-height: 600px;">
</div>
<div class="row mt-5">
    <div class="col-auto" style="width: 20%">
        <div class="row ms-1">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolorum a saepe, quae ut qui ex et facilis quidem suscipit rem minima atque laudantium reprehenderit magni adipisci accusantium tempora est praesentium?
        </div>
        <div class="col-auto mt-3 ms-2">
            <div class="text mb-3 ps-1">
                {{__('Count hall')}}: {{$cinema->halls->count()}}
            </div>
            <ul class="list-group">
                @foreach($cinema->halls as $hall)
                    <li class="list-group-item d-grid"><a class="btn text-start" href="{{route('hall.show', ['hall' => $hall->idHall])}}">{{__('Hall')}} {{$hall->number}}</a></li>
                @endforeach
              </ul>
        </div>
        <div class="col-auto mt-3 ms-2">
            <div class="text mb-3 ps-1">
                {{__('Watch today')}}
            </div>
            <ul class="list-group">
                @foreach($sessions as $index => $session)
                    <li class="list-group-item d-grid"><a class="btn text-start" href="{{route('booking', ['idSession' => $session->idSession])}}">{{__('Session')}} {{$index+1}}</a></li>
                @endforeach
              </ul>
        </div>
        <div class="col-12 d-grid ps-2 mt-4">
            <a href="{{route('timetable', ['cinema' => $cinema->idCinema])}}" class="btn btn-success">{{__('Timetable')}}</a>
        </div>
    </div>
    <div class="col ms-5 me-5">
        <div class="row">
            <div class="col-2 display-6">
                {{$cinema->name}}
            </div>
            <div class="col-3">
                <img src="{{asset($cinema->logo)}}" alt="{{__('Logo')}}" style="width: 90%;">
            </div>
            <div class="col-2 offset-1">
                <a href="{{route('timetable', ['cinema' => $cinema->idCinema])}}" class="btn btn-lg btn-success">{{__('Timetable')}}</a>
            </div>
        </div>
        <div class="row mt-4">
            {{$cinema->description}}
        </div>
        <div class="row mt-4">
            <div class="text-center display-6 mb-4">
                {{__('Conditions')}}
            </div>
            <div class="text-start bg-secondary text-light">
                @foreach($cinema->conditions as $condition)
                    <div class="display-6">
                        {{$condition->name}}
                    </div>
                    <div class="text">
                        {{$condition->description}}
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row mt-4">
            <div class="text-center display-6 mb-4">
                {{__('Photo gallery')}}
            </div>
        </div>
        <div class="row">
            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel" style=" width: 100%; height: 700px; overflow: hidden;">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{isset($cinema->logo) ? asset($cinema->logo) : asset(config("app.defaultPicture"))}}" class="d-block w-100" alt="...">
                    </div>
                    @foreach($cinema->pictures as $picture)
                    <div class="carousel-item">
                        <img src="{{isset($picture->img) ? asset($picture->img) : asset(config("app.defaultPicture"))}}" class="d-block w-100" alt="...">
                    </div>
                    @endforeach
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    </div>
</div>
@endsection