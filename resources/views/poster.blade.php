@extends('layout')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-auto">
            <ul class="nav flex-column nav-pills ms-1">
                <li class="nav-item">
                  <a class="nav-link active" id="curent">{{__('Poster')}}</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="soon">{{__('Soon')}}</a>
                </li>
              </ul>
        </div>
        <div class="col">
            <div class="bg-secondary pt-3 ps-3">
                <div class="row justify-content-start">
                    @foreach($films as $film)
                        <div class="col-auto mb-4 {{$film->status ?? ''}}">
                            <div class="card" style="width: 18rem; height: 25rem;">
                                <img src="{{ $film->mainPicture !== '' ? asset("$film->mainPicture") : asset(config("app.defaultPicture")) }}" class="card-img-top" alt="Image Film" style="width: 18rem; height: 22rem;">
                                <div class="card-body text-center">
                                <a href="{{route('film', ['id' => $film->idFilm])}}">
                                    <p class="card-text">{{$film->name}}</p>
                                </a>
                                <a class="btn btn-success mt-2" href="{{route('timetable', ['film' => $film->idFilm])}}">
                                    <p class="card-text">{{__('Buy ticket')}}</p>
                                </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.soon').each( function(){
        $(this).css("display", "none");
    })
    $('#soon').click(function() {
        $('#soon').addClass('active')
        $('#curent').removeClass('active')
        $('.soon').each( function(){
            $(this).css("display", "block");
        })
        $('.curent').each( function(){
            $(this).css("display", "none");
        })
    });

    $('#curent').click(function () { 
        $(this).addClass('active')
        $('#soon').removeClass('active')
        $('.soon').each( function(){
            $(this).css("display", "none");
        })
        $('.curent').each( function(){
            $(this).css("display", "block");
        })
    });

    @if($soon)
        $('#soon').addClass('active')
        $('#curent').removeClass('active')
        $('.soon').each( function(){
            $(this).css("display", "block");
        })
        $('.curent').each( function(){
            $(this).css("display", "none");
        })
    @endif
</script>
@endsection