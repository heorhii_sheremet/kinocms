@if(Route::current()->getName() === 'home')
  <img src="{{asset('default-images/баннер.jpeg')}}" alt="KinoCMS" style="width: 100%; height: 20px;">
@endif
<nav class="navbar navbar-expand-md navbar-dark container bg-transparent">
  <a href="{{route('home')}}">
    <img src="{{asset('default-images/logo.jpeg')}}" width="200" height="70" alt="KinoCMS">
  </a>
  <form class="form-inline mt-2 mt-md-0 ms-5 d-flex">
    <input class="form-control mr-sm-2" type="text" placeholder="{{__('Search')}}" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0 ms-5" type="submit">{{__('Search')}}</button>
  </form>
  <div class="row align-items-center ms-5">
        <div class="col mt-3">
            <p class="text-primary">Icons</p>
        </div>
        <div class="col mt-3">
            <p class="text-primary">{{isset($data->phone1) ? $data->phone1 : ''}}</p>
            <p class="text-primary">{{isset($data->phone2) ? $data->phone2 : ''}}</p>
        </div>
        <div class="col-12 bg-white pt-3">
            <nav class="nav d-flex justify-content-between">
                <a class="p-2 text-dark text-decoration-none" href="{{route('poster')}}">{{__('Poster')}}</a>
                <a class="p-2 text-dark text-decoration-none" href="{{route('timetable')}}">{{__('Timetable')}}</a>
                <a class="p-2 text-dark text-decoration-none" href="{{route('poster', ['soon' => true])}}">{{__('Soon')}}</a>
                <a class="p-2 text-dark text-decoration-none" href="{{route('cinemas')}}">{{__('Cinemas')}}</a>
                <a class="p-2 text-dark text-decoration-none" href="{{route('stock')}}">{{__('Stock')}}</a>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle text-dark" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">{{__('About')}}</a>
                  <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="{{route('news')}}">{{__('News')}}</a></li>
                    <li><a class="dropdown-item" href="{{route('contacts')}}">{{__('Contacts')}}</a></li>
                    <li><a class="dropdown-item" href="{{route('mobile-app')}}">{{__('Mobile App')}}</a></li>
                    @foreach($navPages as $page)
                      @if($page->state == 1)
                        <li><a class="dropdown-item" href="{{route('page',['page' => $page->idPage])}}">{{$page->name}}</a></li>
                      @endif
                    @endforeach
                  </ul>
                </li>
                <li>
                  <select class="form-select mb-3" id='lang'>
                    <option value="{{route('localization', ['lang' => 'en'])}}" {{session('lang') === 'en' ? 'selected' : ''}}>En</option>
                    <option value="{{route('localization', ['lang' => 'ru'])}}" {{session('lang') === 'ru' ? 'selected' : ''}}>Рус</option>
                  </select>
                </li>
                <div class="col-auto">
                  @if( auth()->user() !== null)
                    <a class=" btn btn-md btn-primary text-light text-decoration-none" href="{{route('account')}}">{{auth()->user()->email}}</a>
                  @else
                    <a class=" btn btn-md btn-primary text-light text-decoration-none" href="{{route('login')}}">{{__('Log in')}}</a>
                  @endif
                </div>  
            </nav>
        </div>
  </div>
</nav>
<script>
  $(document).ready(function(){
    $('#lang').change(function(){
      window.location.href = $(this).val();
    });
  });
  </script>