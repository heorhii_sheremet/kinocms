@extends('layout')
@section('content')
<div class="row">
    <div class="col-9">
        <div class="container">
            <div class="row align-items-start bg-secondary">
                <div class="text-start display-6 mb-4">
                    {{__('News')}}
                </div>
                @foreach($news as $Onenews)
                @if($Onenews->state == '1')
                <div class="col-auto mb-4 mt-2">
                    <div class="card border-secondary" style="width: 23rem; height: 30rem;">
                        <img src="{{ $Onenews->logo !== '' ? asset("$Onenews->logo") : asset(config("app.defaultPicture")) }}" class="card-img-top" alt="Image news" style="width: 23rem; height: 15rem;">
                        <div class="card-body text-start bg-secondary">
                            <a href="{{route('news.show', ['news' => $Onenews->idNews])}}">{{$Onenews->name}}</a>
                            <div class="col-12 mt-2">
                                <span class="text-white bg-dark rounded-pill p-1">{{date('d.m.Y', strtotime($Onenews->publishedDate))}}</span>
                                <span class="text-white bg-info m-1 rounded-pill p-1">{{$Onenews->film->name}}</span>
                            </div>
                            <p class="card-text mt-1">{{$Onenews->description}}</p>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-3 bg-lght pt-5">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id facilis sequi incidunt a molestiae, iusto minima. Autem quod odio nesciunt, fugit dolores alias vitae at obcaecati, eum sed voluptate ex!
    </div>

</div>
@endsection