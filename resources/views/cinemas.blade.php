@extends('layout')
@section('content')
<div class="row">
    <div class="col-9">
        <div class="container">
            <div class="text-center display-6">
                {{__('Our cinemas')}}
            </div>
            <div class="row align-items-start">
                @foreach($cinemas as $cinema)
                    <div class="card m-3 p-0" style="width: 34rem; height: 19rem;">
                        <img src="{{asset($cinema->logo)}}" class="card-img-top" alt="{{__('Image')}}" style="max-width: 100%; height: 19rem">
                        <div class="card-body text-center">
                            <p class="card-text">{{$cinema->name}}</p>
                            <a href="{{route('cinema.show', ['cinema' => $cinema->idCinema])}}" class="stretched-link"></a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-3 bg-lght pt-5">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id facilis sequi incidunt a molestiae, iusto minima. Autem quod odio nesciunt, fugit dolores alias vitae at obcaecati, eum sed voluptate ex!
    </div>

</div>
@endsection