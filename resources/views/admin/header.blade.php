<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <img src="{{asset('default-images/logo.jpeg')}}" width="200" height="70" alt="KinoCMS">
    <div class="d-flex">
      <a class="btn" href="{{route( 'users.edit', ['user' => 1] )}}">
        <img src="{{asset('default-images/person-fill.svg')}}" width="30" height="30" alt="{{ __('My profile') }}">
      </a>
      <a class="btn btn-danger" href="{{route('logout')}}">
        <img src="{{asset('default-images/box-arrow-left.svg')}}" width="30" height="30" alt="{{ __('Logout') }}">
      </a>
    </div>
    </div>
  </div>
</nav>
