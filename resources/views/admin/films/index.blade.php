@extends('admin.layout')
@section('content')
<p class="text-center mt-3">{{__('List of current films')}}</p>
<div class="row align-items-center">
    <div class="col-3" id="btnWrapperCurent">
        <a href="{{route('films.create')}}" class="btn btn-outline-success">
            <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
        </a>
    </div>
</div>
<p class="text-center mt-3">{{__('List of movies coming soon')}}</p>
<div class="row align-items-center">
    <div class="col-3" id="btnWrapperSoon">
        <a href="{{route('films.create')}}" class="btn btn-outline-success">
            <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
        </a>
    </div>
</div>
<script>
    @foreach ($films as $film)
       
        selector = 'btnWrapperCurent'
        console.log(selector)
        if({{$film->status === 'soon' ? 'true' : 'false'}}) selector = 'btnWrapperSoon'
        url ="{{$film->mainPicture}}"
        if (url === '') url = '{{asset(config("app.defaultPicture"))}}'
        else url ="{{asset($film->mainPicture)}}"
        $('#'+selector+'').before('<div class="card m-3 p-0" style="width: 16rem; height: 16rem;">'+
            '<img src="'+url+'" class="card-img-top" alt="{{__('Image')}}" style="max-width: 100%; height: 200px">'+
            '<div class="card-body text-center">'+
                '<p class="card-text">{{$film->name}}</p>'+
                '<a href="{{route('films.edit', ['film' => $film->idFilm])}}" class="stretched-link"></a>'+
            '</div>'+
        '</div>');
    @endforeach
</script>
@endsection
