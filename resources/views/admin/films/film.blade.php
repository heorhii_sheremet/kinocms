@extends('admin.layout')
@section('content')
@isset($film->sessions)

@foreach($film->sessions as $session)

    <form action="{{route('sessions.destroy', ['session' => $session->idSession])}}" method="post" id="delete{{$session->idSession}}">
        @csrf
        @method('DELETE')
    </form>
    @endforeach
@endisset

<div class="container mt-3">
    <form action="{{(isset($film) && $film->idFilm ? route('films.update', ['film' => $film->idFilm]) : route('films.store'))}}" method="POST" enctype = "multipart/form-data" id="filmsForm">
        @csrf
        @isset($film->idFilm)
            @method('PUT')
        @endisset
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Film name')}}</label>
            </div>
            <div class="col-auto">
                <input type="text" name="name" class="form-control" value="{{isset($film->name) ? $film->name : ''}}">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Description')}}</label>
            </div>
            <div class="col-auto">
                <textarea class="form-control" name="description" cols="70" rows="10">{{isset($film->description) ? $film->description : ''}}</textarea>
            </div>
        </div>
        <div class="row align-items-center mb-3" id="logo">
            <div class="col-auto">
                <label class="col-form-label">{{__('Logo')}}</label>
            </div>
            <div class="col-auto">
                <img class="img-fluid rounded" src="{{(isset($film->mainPicture) && $film->mainPicture) ? asset("$film->mainPicture") : asset(config("app.defaultPicture"))}}" style="max-width: 300px; height: 200px" alt="{{ __('logo') }}">
            </div>
            <div class="col-auto">
                <div class="row">
                    <div class="col-auto">
                        <input class="form-control" type="file" name="logo" id="logoImg">
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-danger" type="button" id="deleteLogo">{{__('Delete')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <p>{{__('Picture gallery')}}</p>
            <div class="col-auto">
                <p>{{__('Size')}}: 1000x190</p>
            </div>
        <div class="row align-items-center mb-3" id='gallery'>
            <div class="col-3 mt-2" id="btnWrapper">
                
                <button type="button" class="btn btn-outline-success" id="btn">
                    <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
                </button>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Trailer link')}}</label>
            </div>
            <div class="col-auto">
                <input type="text" name="trailerLink" class="form-control" value="{{isset($film->trailerLink) ? $film->trailerLink : ''}}">
            </div>
        </div>
        <label class="col-form-label">{{__('Movie type')}}</label>
        <div class="form-check form-check-inline ms-3">
          <input class="form-check-input" type="radio" name="type" id="3d" value="3d" {{(isset($film->type) && $film->type === '3d' ? 'checked' : '')}}>
          <label class="form-check-label" for="3d">3D</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="type" id="2d" value="2d" {{(isset($film->type) && $film->type === '2d' ? 'checked' : '')}}>
          <label class="form-check-label" for="2d">2D</label>
        </div>
        <div class="form-check form-check-inline mb-3">
          <input class="form-check-input" type="radio" name="type" id="imax" value="imax" {{(isset($film->type) && $film->type === 'imax' ? 'checked' : '')}}>
          <label class="form-check-label" for="imax">IMAX</label>
        </div>

        @isset($film->idFilm)
            <p class="text-center mt-3 display-6">{{__('List of session')}}</p>
            <div class="row align-items-center mb-3 text-center">
                @isset($film->sessions)
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">{{__('Hall')}}</th>
                            <th scope="col">{{__('Date')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($film->sessions as $session)
                                <tr>
                                    <th scope="row">{{$session->hall->number}} {{__('Hall')}}</th>
                                    <td>{{date('Y-m-d H:i:s', strtotime($session->date))}}</td>
                                    <td class="col-1"><a class="btn btn-primary" href="{{route('sessions.edit', ['session' => $session->idSession])}}">{{__('edit')}}</a></td>
                                    <td class="col-1"><input class="btn btn-danger" type="submit" value="{{__('delete')}}" form="delete{{$session->idSession}}"/></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                  @endisset
                <div class="col mt-2" id="btnWrapperSession">
                    <a href="{{route('films.sessions.create',['film' => $film->idFilm])}}" class="btn btn-outline-success">
                        <img class="mb-1" src="{{asset('default-images/plus-lg.svg')}}" width="20" height="20" alt="{{ __('Add image') }}">
                        {{(__('Create session'))}}
                    </a>
                </div>
            </div>
        @endisset

        <div class="col-3 mb-3">
            <select name="status" class="form-select">
                    <option value="curent">{{__('Curent')}}</option>
                    <option value="soon" {{(isset($film->status) && $film->status === 'soon' ? 'selected' : '')}}>{{__('Soon')}}</option>
            </select>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Price')}}</label>
            </div>
            <div class="col-auto">
                <input type="text" name="price" class="form-control" value="{{isset($film->price) ? $film->price : ''}}">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('SEO block')}}</label>
            </div>
            <div class="col-9">
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('URL:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="url" class="form-control" value="{{ isset($film->SEOBlock->url) ? $film->SEOBlock->url : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Title:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="title" class="form-control" value="{{ isset($film->SEOBlock->title) ? $film->SEOBlock->title : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Keywords:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="keywords" class="form-control" value="{{ isset($film->SEOBlock->keywords) ? $film->SEOBlock->keywords : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Description:')}}</label>
                    </div>
                    <div class="col">
                        <textarea class="form-control" name="SeoDescription" rows="3">{{ isset($film->SEOBlock->description) ? $film->SEOBlock->description : '' }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
            </div>
        </form>
            @isset($film->idFilm)
                <div class="col-auto">
                    <form action="{{route('films.destroy', ['film' => $film->idFilm])}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input class="btn btn-danger mt -5" type="submit" value="{{__('Delete Film')}}"/>
                    </form>
                </div>
            @endisset
        </div>
</div>
<script>
    $('#deleteLogo').on('click', function() {
        $('#filmsForm').append('<input type="hidden" name="deleteLogo">')
        $("#logo").find("img").attr("src","{{asset(config("app.defaultPicture"))}}");
        $('#logoImg').val('')
    })
    index = 0;
    $('#btn').on('click', function() {
        $('#btnWrapper').before('<div class="col-auto text-center mt-5" style="position: relative; max-width: 337px;">'+
                    '<img src="{{asset(config("app.defaultPicture"))}}" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                    '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                    '<input type="file" class="form-control mt-1" name="slides['+index+'][img]" required>'+
                    '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                        '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                    '</button>'+
                '</div>');
            index++;
    })
    @isset($film)
        @foreach ($film->pictures as $slide)
            $('#btnWrapper').before('<div class="col-auto text-center mt-5" style="position: relative; max-width: 337px;">'+
                '<input type="hidden" name="slides['+index+'][id]" value={{$slide->idPicture}}>'+
                '<img src="{{(isset($slide->img) && $slide->img) ? asset("$slide->img") : asset(config("app.defaultPicture"))}}" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                '<input type="file" class="form-control mt-1" name="slides['+index+'][img]">'+
                '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                    '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                '</button>'+
            '</div>');
            index++;
        @endforeach
    @endisset
    $('#gallery').on('click', "#delete", function (){
                $(this).parent().children("#onDelete").attr('value', 'true')
                $(this).parent().attr('style','display: none;')
    });

    $('#logoImg').on('change', function(event) {
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            console.log($(this).closest("#logo").find("img"))
            $(this).closest("#logo").find("img").attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
            $("input[name = 'deleteLogo']").remove()
        }
    })

    $('#gallery').on('change','[type="file"]',function(event){
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            $(this).parent().children('img').attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
        }
    })
    
</script>
<style>
    /* CSS */
    .btn-circle {
        width: 28px;
        height: 28px;
        border-radius: 14px;
        text-align: center;
        padding-left: 0;
        padding-right: 0;
        font-size: 10px;
        background-color: white;
    }
    </style>
@endsection