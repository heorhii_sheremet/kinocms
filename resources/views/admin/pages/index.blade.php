@extends('admin.layout')
@section('content')
@isset($cinema->halls)
@foreach($cinema->halls as $hall)
    <form action="{{route('halls.destroy', ['hall' => $hall->idHall])}}" method="post" id="delete{{$hall->idHall}}">
        @csrf
        @method('DELETE')
    </form>
    @endforeach
@endisset
<div class="row align-items-center">
    <div class="row text-center m-3 display-4">
        <div class="col-10">{{__('List of pages')}}</div>
        <div class="col-2" id="btnWrapper">
            <a href="{{route('pages.create')}}" class="btn btn-outline-success">
                <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
                {{__('Create new page')}}
            </a>
        </div>
    </div>
    <table class="table m-e-5">
        <thead>
          <tr>
            <th scope="col">{{__('Name')}}</th>
            <th scope="col">{{__('Created date')}}</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
        @foreach($pages as $page)
          <tr>
            <th scope="row">{{$page->name}}</th>
            <td>{{$page->created_at}}</td>
            <td class="col-2">
              @if($page->idPage === 1)
                <a class="btn btn-primary" href="{{route('contacts.index')}}">{{__('edit')}}</a>
              @elseif($page->idPage === 2)
                <a class="btn btn-primary" href="{{route('home-page.index')}}">{{__('edit')}}</a>
              @elseif(in_array($page->idPage, [3,4,5,6,7] ))
                <a class="btn btn-primary" href="{{route('pages.edit', ['page' => $page->idPage])}}">{{__('edit')}}</a>
              @else
                <div class="button-group">
                    <form action="{{route('pages.destroy', ['page' => $page->idPage])}}" method="post">
                      <a class="btn btn-primary" href="{{route('pages.edit', ['page' => $page->idPage])}}">{{__('edit')}}</a>
                      @csrf
                      @method('DELETE')
                      <input class="btn btn-danger" type="submit" value="{{__('delete')}}"/>
                    </form>
                </div>
              @endif
            </td>
          </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection

