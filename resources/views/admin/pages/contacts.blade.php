@extends('admin.layout')
@section('content')
<div class="container">
    <form action="{{route('contacts.store')}}" method="POST" enctype = "multipart/form-data">
        @csrf
        <div class="row justify-content-center mt-5" id='wrapper'>
            <div class="col-auto" id="btnWrapper">
                <button type="button" class="btn btn-outline-success" id="btn">
                    <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
                    <label class="mt-1">{{__('Add Cinema')}}</label>
                </button>
            </div>
            <div class="row mb-3 mt-3">
                <div class="col-auto">
                    <label class="col-form-label">{{__('SEO block')}}</label>
                </div>
                <div class="col-9">
                    <div class="row mb-3">
                        <div class="col-auto">
                            <label class="col-form-label">{{__('URL:')}}</label>
                        </div>
                        <div class="col">
                            <input type="text" name="SEOblock[url]" class="form-control" value="{{ isset($SEOBlock->url) ? $SEOBlock->url : '' }}">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-auto">
                            <label class="col-form-label">{{__('Title:')}}</label>
                        </div>
                        <div class="col">
                            <input type="text" name="SEOblock[title]" class="form-control" value="{{ isset($SEOBlock->title) ? $SEOBlock->title : '' }}">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-auto">
                            <label class="col-form-label">{{__('Keywords:')}}</label>
                        </div>
                        <div class="col">
                            <input type="text" name="SEOblock[keywords]" class="form-control" value="{{ isset($SEOBlock->keywords) ? $SEOBlock->keywords : '' }}">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-auto">
                            <label class="col-form-label">{{__('Description:')}}</label>
                        </div>
                        <div class="col">
                            <textarea class="form-control" name="SEOblock[description]" rows="3">{{ isset($SEOBlock->description) ? $SEOBlock->description : '' }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
            </div>
        </div>
    </form>
</div>


<script>
    index = 0
    $('#btn').on('click', function() {
        $('#btnWrapper').before(
            '<div class="form-control mb-3">'+
                '<div class="row mb-3 mt-3">'+
                    '<div class="col-auto">'+
                       ' <label class="col-form-label">{{__('Cinema name')}}</label>'+
                    '</div>'+
                    '<div class="col-auto">'+
                        '<input type="text" name="contacts[' +index+ '][name]" class="form-control">'+
                    '</div>'+
                    '<div class="col-auto ms-auto form-check form-switch form-control-lg">'+
                        '<input class="form-check-input" type="checkbox" name="contacts[' +index+ '][switch]" role="switch">'+
                    '</div>'+
                '</div>'+
                '<div class="row mb-3">'+
                    '<div class="col-auto">'+
                        '<label class="col-form-label">{{__('Address')}}</label>'+
                    '</div>'+
                    '<div class="col-auto">'+
                        '<textarea class="form-control" name="contacts[' +index+ '][address]" cols="70" rows="10"></textarea>'+
                    '</div>'+
                '</div>'+
                '<div class="row mb-3">'+
                    '<div class="col-auto">'+
                        '<label class="col-form-label">{{__('Map coordinates')}}</label>'+
                    '</div>'+
                    '<div class="col-auto">'+
                        '<input type="text" name="contacts[' +index+ '][coordinates]" class="form-control">'+
                    '</div>'+
                '</div>'+
                '<div class="row align-items-center mb-3" id="logo">'+
                    '<div class="col-auto">'+
                        '<label class="col-form-label">{{__('Logo')}}</label>'+
                    '</div>'+
                    '<div class="col-auto">'+
                        '<img class="img-fluid rounded" src="{{asset(config("app.defaultPicture"))}}" style="max-width: 300px; height: 200px" alt="{{ __('logo') }}">'+
                    '</div>'+
                    '<div class="col-auto">'+
                        '<div class="row">'+
                            '<div class="col-auto">'+
                                '<input class="form-control" type="file" name="contacts[' +index+ '][logo]" id="logoImg" required>'+
                            '</div>'+
                            '<div class="col-auto">'+
                                '<button class="btn btn-danger" type="button" id="deleteLogo">{{__('Delete')}}</button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>')
            index++;
    })

    @foreach($contacts as $contact)
        console.log('here')
        $('#btnWrapper').before(
            '<div class="form-control mb-3">'+
                '<div class="row mb-3 mt-3">'+
                    '<div class="col-auto">'+
                       ' <label class="col-form-label">{{__('Cinema name')}}</label>'+
                    '</div>'+
                    '<div class="col-auto">'+
                        '<input type="text" name="contacts[' +index+ '][name]" class="form-control" value="{{$contact->name}}">'+
                        '<input type="hidden" name="contacts[' +index+ '][idContact]" class="form-control contact" value="{{$contact->idContact}}">'+
                    '</div>'+
                    '<div class="col-auto ms-auto form-check form-switch form-control-lg">'+
                        '<input class="form-check-input" type="checkbox" name="contacts[' +index+ '][switch]" role="switch" {{$contact->switch == 1 ? 'checked' : ''}}>'+
                    '</div>'+
                '</div>'+
                '<div class="row mb-3">'+
                    '<div class="col-auto">'+
                        '<label class="col-form-label">{{__('Address')}}</label>'+
                    '</div>'+
                    '<div class="col-auto">'+
                        '<textarea class="form-control" name="contacts[' +index+ '][address]" cols="70" rows="10"></textarea>'+
                    '</div>'+
                '</div>'+
                '<div class="row mb-3">'+
                    '<div class="col-auto">'+
                        '<label class="col-form-label">{{__('Map coordinates')}}</label>'+
                    '</div>'+
                    '<div class="col-auto">'+
                        '<input type="text" name="contacts[' +index+ '][coordinates]" class="form-control" value="{{$contact->coordinates}}">'+
                    '</div>'+
                '</div>'+
                '<div class="row align-items-center mb-3" id="logo">'+
                    '<div class="col-auto">'+
                        '<label class="col-form-label">{{__('Logo')}}</label>'+
                    '</div>'+
                    '<div class="col-auto">'+
                        '<img class="img-fluid rounded" src="{{isset($contact->logo) ? asset("$contact->logo") : asset(config("app.defaultPicture"))}}" style="max-width: 300px; height: 200px" alt="{{ __('logo') }}">'+
                    '</div>'+
                    '<div class="col-auto">'+
                        '<div class="row">'+
                            '<div class="col-auto">'+
                                '<input class="form-control" type="file" name="contacts[' +index+ '][logo]" id="logoImg">'+
                            '</div>'+
                            '<div class="col-auto">'+
                                '<button class="btn btn-danger" type="button" id="deleteLogo">{{__('Delete')}}</button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>')
        $(' textarea[name ="contacts[' +index+ '][address]"]').text("{{preg_replace("/[\r\n]+/", "\\n", $contact->address)}}")
        index++;
    @endforeach
    

    $('#wrapper').on('click', "#deleteLogo", function (){
        $(this).closest("#logo").find("img").attr("src","{{asset(config("app.defaultPicture"))}}");
        $(this).closest("#logo").find("#logoImg").val('')
    })
    $('#deleteLogo').on('click', function() {
        $('#filmsForm').append('<input type="hidden" name="deleteLogo">')
        $("#logo").find("img").attr("src","{{asset(config("app.defaultPicture"))}}");
        $('#logoImg').val('').prop('required',true);
    })                   


    $('#wrapper').on('change', "#logoImg", function (event){
        var files = event.target.files, file;
        if (files && files.length > 0) {
            console.log('here')
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            console.log($(this).closest("#logo").find("img"))
            $(this).closest("#logo").find("img").attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
        }
    })
</script>
   
@endsection
