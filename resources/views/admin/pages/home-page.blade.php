@extends('admin.layout')
@section('content')
<div class="container">
    <form action="{{route('home-page.store')}}" method="POST">
        @csrf
        <div class="row justify-content-center mt-5">
            <div class="row mb-3">
                <div class="col-auto">
                    <label class="col-form-label">{{__('Phone 1')}}</label>
                </div>
                <div class="col">
                    <input type="text" name="phone1" class="form-control" value="{{ isset($data->phone1) ? $data->phone1 : '' }}">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-auto">
                    <label class="col-form-label">{{__('Phone 2')}}</label>
                </div>
                <div class="col">
                    <input type="text" name="phone2" class="form-control" value="{{ isset($data->phone2) ? $data->phone2 : '' }}">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-auto">
                    <label class="col-form-label">{{__('Seo text:')}}</label>
                </div>
                <div class="col">
                    <textarea class="form-control" name="seoText" rows="3">{{ isset($data->seoText) ? $data->seoText : '' }}</textarea>
                </div>
            </div>
            <div class="row mb-3 mt-3">
                <div class="col-auto">
                    <label class="col-form-label">{{__('SEO block')}}</label>
                </div>
                <div class="col-9">
                    <div class="row mb-3">
                        <div class="col-auto">
                            <label class="col-form-label">{{__('URL:')}}</label>
                        </div>
                        <div class="col">
                            <input type="text" name="SEOblock[url]" class="form-control" value="{{ isset($SEOBlock->url) ? $SEOBlock->url : '' }}">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-auto">
                            <label class="col-form-label">{{__('Title:')}}</label>
                        </div>
                        <div class="col">
                            <input type="text" name="SEOblock[title]" class="form-control" value="{{ isset($SEOBlock->title) ? $SEOBlock->title : '' }}">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-auto">
                            <label class="col-form-label">{{__('Keywords:')}}</label>
                        </div>
                        <div class="col">
                            <input type="text" name="SEOblock[keywords]" class="form-control" value="{{ isset($SEOBlock->keywords) ? $SEOBlock->keywords : '' }}">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-auto">
                            <label class="col-form-label">{{__('Description:')}}</label>
                        </div>
                        <div class="col">
                            <textarea class="form-control" name="SEOblock[description]" rows="3">{{ isset($SEOBlock->description) ? $SEOBlock->description : '' }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
            </div>
        </div>
    </form>
</div>

@endsection


   

