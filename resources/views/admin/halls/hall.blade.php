@extends('admin.layout')
@section('content')
<div class="container mt-3">
    
    <form action="{{(isset($hall) && $hall->idHall ? route('halls.update', ['hall' => $hall->idHall]) : route('cinemas.halls.store', ['cinema' => $idCinema]))}}" method="POST" enctype = "multipart/form-data" id="hallsForm">
        @csrf
        @isset($hall->idHall)
            @method('PUT')
        @endisset
        <input type="hidden" name="idCinema" value="{{$idCinema}}">
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Hall number')}}</label>
            </div>
            <div class="col-auto">
                <input type="text" name="number" class="form-control" value="{{isset($hall->number) ? $hall->number : ''}}">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Description hall')}}</label>
            </div>
            <div class="col-auto">
                <textarea class="form-control" name="description" cols="70" rows="10">{{isset($hall->description) ? $hall->description : ''}}</textarea>
            </div>
        </div>
        
        <div class="row align-items-center mb-3" id="scheme">
            <div class="col-auto">
                <label class="col-form-label">{{__('Scheme')}}</label>
            </div>
            <div class="col-auto">
                <img class="img-fluid rounded" src="{{(isset($hall->scheme) && $hall->scheme) ? asset("$hall->scheme") : asset(config("app.defaultPicture"))}}" style="max-width: 300px; height: 200px" alt="{{ __('logo') }}">
            </div>
            <div class="col-auto">
                <div class="row">
                    <div class="col-auto">
                        <input class="form-control" type="file" name="scheme" id="SchemeImg">
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-danger" type="button" id="deleteScheme">{{__('Delete')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row align-items-center mb-3" id="banner">
            <div class="col-auto">
                <label class="col-form-label">{{__('Banner')}}</label>
            </div>
            <div class="col-auto">
                <img class="img-fluid rounded" src="{{(isset($hall->topBanner) && $hall->topBanner) ? asset("$hall->topBanner") : asset(config("app.defaultPicture"))}}" style="max-width: 300px; height: 200px" alt="{{ __('logo') }}">
            </div>
            <div class="col-auto">
                <div class="row">
                    <div class="col-auto">
                        <input class="form-control" type="file" name="banner" id="BannerImg">
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-danger" type="button" id="deleteBanner">{{__('Delete')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <p>{{__('Picture gallery')}}</p>
            <div class="col-auto">
                <p>{{__('Size')}}: 1000x190</p>
            </div>
        <div class="row align-items-center mb-3" id='gallery'>
            <div class="col-3 mt-2" id="btnWrapper">
                
                <button type="button" class="btn btn-outline-success" id="btn">
                    <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
                </button>
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('SEO block')}}</label>
            </div>
            <div class="col-9">
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('URL:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="SEOblock[url]" class="form-control" value="{{ isset($hall->SEOBlock->url) ? $hall->SEOBlock->url : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Title:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="SEOblock[title]" class="form-control" value="{{ isset($hall->SEOBlock->title) ? $hall->SEOBlock->title : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Keywords:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="SEOblock[keywords]" class="form-control" value="{{ isset($hall->SEOBlock->keywords) ? $hall->SEOBlock->keywords : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Description:')}}</label>
                    </div>
                    <div class="col">
                        <textarea class="form-control" name="SEOblock[description]" rows="3">{{ isset($hall->SEOBlock->description) ? $hall->SEOBlock->description : '' }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
            </div>
        </form>
        </div>
</div>
<script>
    $('#deleteScheme').on('click', function() {
        $('#hallsForm').append('<input type="hidden" name="deleteScheme">')
        $("#scheme").find("img").attr("src","{{asset(config("app.defaultPicture"))}}");
        $('#schemeImg').val('')
    })
    $('#deleteBanner').on('click', function() {
        $('#hallsForm').append('<input type="hidden" name="deleteBanner">')
        $("#banner").find("img").attr("src","{{asset(config("app.defaultPicture"))}}");
        $('#BannerImg').val('')
    })
    index = 0;
    $('#btn').on('click', function() {
        $('#btnWrapper').before('<div class="col-auto text-center mt-5" style="position: relative; max-width: 337px;">'+
                    '<img src="{{asset(config("app.defaultPicture"))}}" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                    '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                    '<input type="file" class="form-control mt-1" name="slides['+index+'][img]" required>'+
                    '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                        '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                    '</button>'+
                '</div>');
            index++;
    })
    @isset($hall)
    
        @foreach ($hall->pictures as $slide)
            $('#btnWrapper').before('<div class="col-auto text-center mt-5" style="position: relative; max-width: 337px;">'+
                '<input type="hidden" name="slides['+index+'][id]" value={{$slide->idPicture}}>'+
                '<img src="{{(isset($slide->img) && $slide->img) ? asset("$slide->img") : asset(config("app.defaultPicture"))}}" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                '<input type="file" class="form-control mt-1" name="slides['+index+'][img]">'+
                '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                    '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                '</button>'+
            '</div>');
            index++;
        @endforeach
    @endisset
    $('#gallery').on('click', "#delete", function (){
                $(this).parent().children("#onDelete").attr('value', 'true')
                $(this).parent().attr('style','display: none;')
    });

    $('#SchemeImg').on('change', function(event) {
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            console.log($(this).closest("#scheme").find("img"))
            $(this).closest("#scheme").find("img").attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
            $("input[name = 'deleteScheme']").remove()
        }
    })

    $('#BannerImg').on('change', function(event) {
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            console.log($(this).closest("#banner").find("img"))
            $(this).closest("#banner").find("img").attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
            $("input[name = 'deleteBanner']").remove()
        }
    })

    $('#gallery').on('change','[type="file"]',function(event){
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            $(this).parent().children('img').attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
        }
    })
    
</script>
<style>
    /* CSS */
    .btn-circle {
        width: 28px;
        height: 28px;
        border-radius: 14px;
        text-align: center;
        padding-left: 0;
        padding-right: 0;
        font-size: 10px;
        background-color: white;
    }
    </style>
@endsection