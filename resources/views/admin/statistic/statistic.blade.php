@extends('admin.layout')
@section('content')
<div class="row me-2 mt-2">
  <div class="col-6">
    <div class="small-box bg-warning">
      <div class="inner">
        <h3>{{$users->count()}}</h3>
        <p>User Registrations</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <a href="{{route('users.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
  </div>
  </div>
  <div class="col-6">
    <div class="card card-primary card-outline">
      <div class="card-header">
        <h3 class="card-title">
          <i class="far fa-chart-bar"></i>
          {{__("Sessions")}}
        </h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
      <div id="session-graph" style="height: 300px; padding: 0px; position: relative;"><canvas class="flot-base" width="404" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 404.5px; height: 300px;"></canvas><canvas class="flot-overlay" width="404" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 404.5px; height: 300px;"></canvas><div class="flot-svg" style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; pointer-events: none;"><svg style="width: 100%; height: 100%;"><g class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; inset: 0px;"><text x="37" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">0</text><text x="89.66666666666666" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">4</text><text x="142.33333333333331" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">4</text><text x="195" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">6</text><text x="296.3572591145833" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">10</text><text x="349.02392578125" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">12</text><text x="247.66666666666666" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">8</text></g><g class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; inset: 0px;"><text x="1" y="269" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">-1.5</text><text x="1" y="226.66666666666669" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">-1.0</text><text x="1" y="184.33333333333334" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">-0.5</text><text x="5.9765625" y="15" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">1.5</text><text x="5.9765625" y="142" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">0.0</text><text x="5.9765625" y="99.66666666666667" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">0.5</text><text x="5.9765625" y="57.333333333333336" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">1.0</text></g></svg></div></div>
      </div>
    </div>
  </div>
</div>
<div class="row mt-2 ms-2 me-3">
  <div class="card card-primary card-outline">
    <div class="card-header">
      <h3 class="card-title">
        <i class="far fa-chart-bar"></i>
        {{__('Device visitors')}}
      </h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
      <div id="visitor-graph" style="height: 300px; padding: 0px; position: relative;"><canvas class="flot-base" width="404" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 404.5px; height: 300px;"></canvas><canvas class="flot-overlay" width="404" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 404.5px; height: 300px;"></canvas><div class="flot-svg" style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; pointer-events: none;"><svg style="width: 100%; height: 100%;"><g class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; inset: 0px;"><text x="37" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">0</text><text x="89.66666666666666" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">4</text><text x="142.33333333333331" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">4</text><text x="195" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">6</text><text x="296.3572591145833" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">10</text><text x="349.02392578125" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">12</text><text x="247.66666666666666" y="294" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">8</text></g><g class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; inset: 0px;"><text x="1" y="269" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">-1.5</text><text x="1" y="226.66666666666669" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">-1.0</text><text x="1" y="184.33333333333334" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">-0.5</text><text x="5.9765625" y="15" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">1.5</text><text x="5.9765625" y="142" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">0.0</text><text x="5.9765625" y="99.66666666666667" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">0.5</text><text x="5.9765625" y="57.333333333333336" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">1.0</text></g></svg></div></div>
    </div>
  </div>
</div>
<div class="row ms-2 me-3">
  <div class="card card-danger p-0">
    <div class="card-header">
      <h3 class="card-title">{{__('Gender')}}</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
      <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 380px;" width="380" height="250" class="chartjs-render-monitor"></canvas>
    </div>  
  </div>
</div>
<script src="adminLTE/plugins/jquery/jquery.min.js"></script>

<script src="adminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="adminLTE/dist/js/adminlte.min.js?v=3.2.0"></script>

<script src="adminLTE/plugins/flot/jquery.flot.js"></script>

<script src="adminLTE/plugins/flot/plugins/jquery.flot.resize.js"></script>

<script src="adminLTE/plugins/flot/plugins/jquery.flot.pie.js"></script>

<script src="adminLTE/plugins/chart.js/Chart.min.js"></script>
<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="..//adminLTE/plugins/fontawesome-free/css/all.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet" href="..//adminLTE/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="..//adminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- JQVMap -->
<link rel="stylesheet" href="..//adminLTE/plugins/jqvmap/jqvmap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="..//adminLTE/dist/css/adminlte.min.css">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="..//adminLTE/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="..//adminLTE/plugins/daterangepicker/daterangepicker.css">
<!-- summernote -->
<link rel="stylesheet" href="..//adminLTE/plugins/summernote/summernote-bs4.min.css">   





<script>
  $(function () {
    /*
     * LINE CHART
     * ----------
     */
    //LINE randomly generated data

    data =[]
    ticksX = []
    ticksY = []
    @foreach($sessions as $date => $count)
      data.push([{{date(strtotime($date))}}, {{$count}}])
      ticksX.push([{{date(strtotime($date))}}, "{{date('d M', strtotime($date))}}"])
      ticksY.push({{$count}}, "{{$count}}")
    @endforeach
    
    var line_data1 = {
      data : data,
      color: '#3c8dbc'
    }

    $.plot('#session-graph', [line_data1], {
      grid  : {
        hoverable  : true,
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor  : '#f3f3f3'
      },
      series: {
        shadowSize: 0,
        lines     : {
          show: true
        },
        points    : {
          show: true
        }
      },
      lines : {
        fill : false,
        color: ['#3c8dbc', '#f56954']
      },
      yaxis : {
        ticks: ticksY
      },
      xaxis : {
        ticks: ticksX
      }
    })
    
    /* END LINE CHART */

    /*
     * Device visitors
     * ----------
     */

    mobVisitors = []
    desktopVisitors = []
    ticksX = []
    ticksY = []
    
    @foreach($mobileVisitorsbyDate as $index => $row)
      mobVisitors.push([{{date(strtotime($row->date))}}, {{$row->count}}])
      ticksX.push([{{date(strtotime($row->date))}}, "{{date('d M', strtotime($row->date))}}"])
      ticksY.push({{$row->count}}, "{{$row->count}}")
    @endforeach

    @foreach($desktopVisitorsbyDate as $index => $row)
    desktopVisitors.push([{{date(strtotime($row->date))}}, {{$row->count}}])
      ticksX.push([{{date(strtotime($row->date))}}, "{{date('d M', strtotime($row->date))}}"])
      ticksY.push({{$row->count}}, "{{$row->count}}")
    @endforeach
  
    var line_data1 = {
      data : mobVisitors,
      color: '#3c8dbc'
    }
    var line_data2 = {
      data : desktopVisitors,
      color: '#2c22bc'
    }
    

    $.plot('#visitor-graph', [line_data1, line_data2], {
      grid  : {
        hoverable  : true,
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor  : '#f3f3f3'
      },
      series: {
        shadowSize: 0,
        lines     : {
          show: true
        },
        points    : {
          show: true
        }
      },
      lines : {
        fill : false,
        color: ['#3c8dbc', '#f56954']
      },
      yaxis : {
        ticks: ticksY
      },
      xaxis : {
        ticks: ticksX
      }
    })
    
    /* END LINE CHART */

  })
  $(function () {
    
    var donutData = {
      labels: [
          '{{$users->where('gender', 'man')->count() / $users->count() * 100}} % {{__('Man')}}',
          '{{$users->where('gender', 'woman')->count() / $users->count() * 100}} % {{__('Woman')}}',
      ],
      datasets: [
        {
          data: [{{$users->where('gender', 'man')->count()}}, {{$users->where('gender', 'woman')->count()}}],
          backgroundColor : ['#f56954', '#00a65a'],
        }
      ]
    }
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = donutData;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

  })
  

</script>
@endsection
