@extends('admin.layout')
@section('content')

<p class="text-center mt-3">{{__('List of cinemas')}}</p>
<div class="row align-items-center">
    <div class="col-3" id="btnWrapper">
        <a href="{{route('cinemas.create')}}" class="btn btn-outline-success">
            <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
        </a>
    </div>
</div>
<script>
    @foreach ($cinemas as $cinema)
        url ="{{$cinema->logo}}"
        if (url === '') url = '{{asset(config("app.defaultPicture"))}}'
        else url ="{{asset($cinema->logo)}}"
        $('#btnWrapper').before('<div class="card m-3 p-0" style="width: 16rem; height: 16rem;">'+
            '<img src="'+url+'" class="card-img-top" alt="{{__('Image')}}" style="max-width: 100%; height: 200px">'+
            '<div class="card-body text-center">'+
                '<p class="card-text">{{$cinema->name}}</p>'+
                '<a href="{{route('cinemas.edit', ['cinema' => $cinema->idCinema])}}" class="stretched-link"></a>'+
            '</div>'+
        '</div>');
    @endforeach
</script>
@endsection
