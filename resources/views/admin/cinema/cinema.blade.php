@extends('admin.layout')
@section('content')

@isset($cinema->halls)
@foreach($cinema->halls as $hall)
    <form action="{{route('halls.destroy', ['hall' => $hall->idHall])}}" method="post" id="delete{{$hall->idHall}}">
        @csrf
        @method('DELETE')
    </form>
    @endforeach
@endisset

<div class="container mt-3">
    <form action="{{(isset($cinema) && $cinema->idCinema ? route('cinemas.update', ['cinema' => $cinema->idCinema]) : route('cinemas.store'))}}" method="POST" enctype = "multipart/form-data" id="cinemasForm">
        @csrf
        @isset($cinema->idCinema)
            @method('PUT')
        @endisset
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Cinema name')}}</label>
            </div>
            <div class="col-auto">
                <input type="text" name="name" class="form-control" value="{{isset($cinema->name) ? $cinema->name : ''}}">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Description')}}</label>
            </div>
            <div class="col-auto">
                <textarea class="form-control" name="description" cols="70" rows="10">{{isset($cinema->description) ? $cinema->description : ''}}</textarea>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Conditions')}}</label>
            </div>
            <div class="col-auto">
                <select class="form-select" name="conditions[]" multiple>
                    @foreach($conditions as $condition)
                        <option value="{{$condition->idCondition}}"
                            @isset($cinema->conditions)
                                {{$cinema->conditions->contains(function ($conditionsCinema, $key) use($condition) {
                                    return $conditionsCinema->idCondition === $condition->idCondition;
                                }) ? 'selected' : ''}}
                            @endisset
                        > {{$condition->name}} </option>
                    @endforeach
                  </select>
            </div>
        </div>
        <div class="row align-items-center mb-3" id="logo">
            <div class="col-auto">
                <label class="col-form-label">{{__('Logo')}}</label>
            </div>
            <div class="col-auto">
                <img class="img-fluid rounded" src="{{(isset($cinema->logo) && $cinema->logo) ? asset("$cinema->logo") : asset(config("app.defaultPicture"))}}" style="max-width: 300px; height: 200px" alt="{{ __('logo') }}">
            </div>
            <div class="col-auto">
                <div class="row">
                    <div class="col-auto">
                        <input class="form-control" type="file" name="logo" id="logoImg">
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-danger" type="button" id="deleteLogo">{{__('Delete')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row align-items-center mb-3" id="banner">
            <div class="col-auto">
                <label class="col-form-label">{{__('Banner')}}</label>
            </div>
            <div class="col-auto">
                <img class="img-fluid rounded" src="{{(isset($cinema->banner) && $cinema->banner) ? asset("$cinema->banner") : asset(config("app.defaultPicture"))}}" style="max-width: 300px; height: 200px" alt="{{ __('logo') }}">
            </div>
            <div class="col-auto">
                <div class="row">
                    <div class="col-auto">
                        <input class="form-control" type="file" name="banner" id="BannerImg">
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-danger" type="button" id="deleteBanner">{{__('Delete')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <p>{{__('Picture gallery')}}</p>
            <div class="col-auto">
                <p>{{__('Size')}}: 1000x190</p>
            </div>
        <div class="row align-items-center mb-3" id='gallery'>
            <div class="col-3 mt-2" id="btnWrapper">
                
                <button type="button" class="btn btn-outline-success" id="btn">
                    <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
                </button>
            </div>
        </div>
        @isset($cinema->idCinema)
            <p class="text-center mt-3 display-6">{{__('List of halls')}}</p>
            <div class="row align-items-center mb-3 text-center">
                @isset($cinema->halls)
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">{{__('Name')}}</th>
                            <th scope="col">{{__('Created date')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($cinema->halls as $hall)
                                <tr>
                                    <th scope="row">{{$hall->number}} {{__('Hall')}}</th>
                                    <td>{{$hall->created_at}}</td>
                                    <td class="col-1"><a class="btn btn-primary" href="{{route('halls.edit', ['hall' => $hall->idHall])}}">{{__('edit')}}</a></td>
                                    <td class="col-1"><input class="btn btn-danger" type="submit" value="{{__('delete')}}" form="delete{{$hall->idHall}}"/></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                  @endisset
                <div class="col mt-2" id="btnWrapperHall">
                    <a href="{{route('cinemas.halls.create',['cinema' => $cinema->idCinema])}}" class="btn btn-outline-success">
                        <img class="mb-1" src="{{asset('default-images/plus-lg.svg')}}" width="20" height="20" alt="{{ __('Add image') }}">
                        Create hall
                    </a>
                </div>
            </div>
        @endisset
        
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('SEO block')}}</label>
            </div>
            <div class="col-9">
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('URL:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="SEOblock[url]" class="form-control" value="{{ isset($cinema->SEOBlock->url) ? $cinema->SEOBlock->url : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Title:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="SEOblock[title]" class="form-control" value="{{ isset($cinema->SEOBlock->title) ? $cinema->SEOBlock->title : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Keywords:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="SEOblock[keywords]" class="form-control" value="{{ isset($cinema->SEOBlock->keywords) ? $cinema->SEOBlock->keywords : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Description:')}}</label>
                    </div>
                    <div class="col">
                        <textarea class="form-control" name="SEOblock[description]" rows="3">{{ isset($cinema->SEOBlock->description) ? $cinema->SEOBlock->description : '' }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
            </div>
        </form>
            @isset($cinema->idCinema)
                <div class="col-auto">
                    <form action="{{route('cinemas.destroy', ['cinema' => $cinema->idCinema])}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input class="btn btn-danger mt -5" type="submit" value="{{__('Delete Cinema')}}"/>
                    </form>
                </div>
            @endisset
        </div>
</div>
<script>
    $('#deleteLogo').on('click', function() {
        $('#cinemasForm').append('<input type="hidden" name="deleteLogo">')
        $("#logo").find("img").attr("src","{{asset(config("app.defaultPicture"))}}");
        $('#logoImg').val('')
    })
    $('#deleteBanner').on('click', function() {
        $('#cinemasForm').append('<input type="hidden" name="deleteBanner">')
        $("#banner").find("img").attr("src","{{asset(config("app.defaultPicture"))}}");
        $('#BannerImg').val('')
    })
    index = 0;
    $('#btn').on('click', function() {
        $('#btnWrapper').before('<div class="col-auto text-center mt-5" style="position: relative; max-width: 337px;">'+
                    '<img src="{{asset(config("app.defaultPicture"))}}" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                    '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                    '<input type="file" class="form-control mt-1" name="slides['+index+'][img]" required>'+
                    '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                        '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                    '</button>'+
                '</div>');
            index++;
    })
    @isset($cinema)
        @foreach ($cinema->pictures as $slide)
            $('#btnWrapper').before('<div class="col-auto text-center mt-5" style="position: relative; max-width: 337px;">'+
                '<input type="hidden" name="slides['+index+'][id]" value={{$slide->idPicture}}>'+
                '<img src="{{(isset($slide->img) && $slide->img) ? asset("$slide->img") : asset(config("app.defaultPicture"))}}" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                '<input type="file" class="form-control mt-1" name="slides['+index+'][img]">'+
                '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                    '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                '</button>'+
            '</div>');
            index++;
        @endforeach
    @endisset
    $('#gallery').on('click', "#delete", function (){
                $(this).parent().children("#onDelete").attr('value', 'true')
                $(this).parent().attr('style','display: none;')
    });

    $('#logoImg').on('change', function(event) {
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            console.log($(this).closest("#logo").find("img"))
            $(this).closest("#logo").find("img").attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
            $("input[name = 'deleteLogo']").remove()
        }
    })

    $('#BannerImg').on('change', function(event) {
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            console.log($(this).closest("#banner").find("img"))
            $(this).closest("#banner").find("img").attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
            $("input[name = 'deleteBanner']").remove()
        }
    })

    $('#gallery').on('change','[type="file"]',function(event){
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            $(this).parent().children('img').attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
        }
    })
    
</script>
<style>
    /* CSS */
    .btn-circle {
        width: 28px;
        height: 28px;
        border-radius: 14px;
        text-align: center;
        padding-left: 0;
        padding-right: 0;
        font-size: 10px;
        background-color: white;
    }
    </style>
@endsection