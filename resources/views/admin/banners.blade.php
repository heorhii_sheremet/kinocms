@extends('admin.layout')
@section('content')
    <p class="text-center">{{__('Home page in top')}}</p>
    <div class="form-control">
        <form action="{{route('save.slider.top')}}" enctype = "multipart/form-data" method="POST" class="pt-3" id="topSliderForm">
            @csrf
            <p>{{__('Size')}}: 1000x190</p>
            <div class="row align-items-center" id='containerTop'>
                <div class="col-3" id="btnWrapper">
                    <button type="button" class="btn btn-outline-success" id="btn">
                        <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
                    </button>
                </div>
            </div>
            <div class="row mt-3 mb-3">
                <div class="col-auto">
                    <label class="col-form-label">{{__('Rotational speed')}}</label>
                </div>
                <div class="col-auto">
                    <select name="rotationalSpeed" class="form-select" id = "selectRotationalSpeedTop">
                        @foreach ([1,2,3,4,5,10,20] as $speed)
                            @if($speed === $settingHomePage->topRotationalSpeed)
                                <option value="{{$speed}}" selected>{{$speed}}{{__('s')}}</option>
                            @else
                                <option value="{{$speed}}">{{$speed}}{{__('s')}}</option>
                            @endif
                        @endforeach
                    
                    </select>
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-success">{{__('Save')}}</button>
                </div>
            </div>
        </form>
    </div>
    <p class="text-center pt-5">{{__('See-through banner on the background')}}</p>
    <div class="form-control">
        <form action="{{route('save.banner')}}" enctype = "multipart/form-data" method="POST" id = "banner">
            @csrf
            <p>{{__('Size')}}: 2000x3000</p>
            <div class="row align-items-center" id="BannerWrapper">
                <div class="col-auto">
                    <img class="img-fluid rounded" src="
                    @if($settingHomePage->backgroundBanner !== '') {{asset($settingHomePage->backgroundBanner)}}
                    @else {{asset(config("app.defaultPicture"))}}
                    @endif
                    "  style="max-width: 300px; height: 200px" alt="{{ __('Banner') }}">
                </div>
                <div class="col-auto">
                    <div class="row">
                        <div class="col-auto">
                            <input class="form-control" type="file" name="banner" id="bannerImg">
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-primary">{{__('Add')}}</button>
                        </div>
                        <div class="col-auto">
                            <a class="btn btn-danger" href="{{route('delete.banner')}}">{{__('Delete')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </form> 
    </div>
    <p class="text-center pt-5">{{__('See-through banner on the background')}}</p>
    <div class="form-control">
        <form action="{{route('save.slider.news')}}" enctype = "multipart/form-data" method="POST" class="pt-3" id="newsSliderForm">
            @csrf
            <p>{{__('Size')}}: 1000x190</p>
            <div class="row align-items-center" id='containerNews'>
                <div class="col-3" id="WrapperNews">
                <button type="button" class="btn btn-outline-success" id="btnNews">
                        <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
                    </button>
                </div>
            </div>
            <div class="row mt-3 mb-3">
                <div class="col-auto">
                    <label class="col-form-label">{{__('Rotational speed')}}</label>
                </div>
                <div class="col-auto">
                    <select name="rotationalSpeedNews" class="form-select" id = "selectRotationalSpeedNews">
                        @foreach ([1,2,3,4,5,10,20] as $speed)
                            @if($speed === $settingHomePage->newsRotationalSpeed)
                                <option value="{{$speed}}" selected>{{$speed}}{{__('s')}}</option>
                            @else
                                <option value="{{$speed}}">{{$speed}}{{__('s')}}</option>
                            @endif
                        @endforeach
                    
                    </select>
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-success">{{__('Save')}}</button>
                </div>
            </div>
        </form>
    </div>
<script>
    $('#bannerImg').on('change', function(event) {
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            console.log($(this).closest("#BannerWrapper").find("img"))
            $(this).closest("#BannerWrapper").find("img").attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
        }
    })
    index = 0;
    $('#btn').on('click', function() {
        $('#btnWrapper').before('<div class="col-auto text-center mb-5" style="position: relative; max-width: 357px;">'+
                    '<img src="{{asset(config("app.defaultPicture"))}}" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                    '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                    '<input type="file" class="form-control mt-1" name="slides['+index+'][img]" required>'+
                    '<input type="text" class="form-control mt-1" name="slides['+index+'][text]" placeholder="text">'+
                    '<input type="text" class="form-control mt-1" name="slides['+index+'][url]" placeholder="url">'+
                    '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                        '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                    '</button>'+
                '</div>');
            index++;
    })
    $('#btnNews').on('click', function() {
        $('#WrapperNews').before('<div class="col-auto text-center mb-5" style="position: relative; max-width: 357px;">'+
                    '<img src="{{asset(config("app.defaultPicture"))}}" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                    '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                    '<input type="file" class="form-control mt-1" name="slides['+index+'][img]" required>'+
                    '<input type="text" class="form-control mt-1" name="slides['+index+'][text]" placeholder="text">'+
                    '<input type="text" class="form-control mt-1" name="slides['+index+'][url]" placeholder="url">'+
                    '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                        '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                    '</button>'+
                '</div>');
            index++;
    })
    

    @foreach ($slidesTop as $slide)
        url ="{{$slide->img}}"
        if (url === '') url = '{{asset(config("app.defaultPicture"))}}'
        else url ="{{asset($slide->img)}}"
        $('#btnWrapper').before('<div class="col-auto text-center mb-5" style="position: relative; max-width: 357px;">'+
                    '<img src="'+url+'" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                    '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                    '<input type="hidden" name="slides['+index+'][id]" value={{$slide->idPicture}}>'+
                    '<input type="file" class="form-control mt-1" name="slides['+index+'][img]">'+
                    '<input type="text" class="form-control mt-1" name="slides['+index+'][text]" placeholder="text" value="{{$slide->text}}">'+
                    '<input type="text" class="form-control mt-1" name="slides['+index+'][url]" placeholder="url" value="{{$slide->url}}">'+
                    '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                        '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                    '</button>'+
                '</div>');
            index++;
    @endforeach
    
    @foreach ($slidesNews as $slide)
        url ="{{$slide->img}}"
        if (url === '') url = '{{asset(config("app.defaultPicture"))}}'
        else url ="{{asset($slide->img)}}"
        $('#WrapperNews').before('<div class="col-auto text-center mb-5" style="position: relative; max-width: 357px;">'+
                    '<img src="'+url+'" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                    '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                    '<input type="hidden" name="slides['+index+'][id]" value={{$slide->idPicture}}>'+
                    '<input type="file" class="form-control mt-1" name="slides['+index+'][img]">'+
                    '<input type="text" class="form-control mt-1" name="slides['+index+'][text]" placeholder="text" value="{{$slide->text}}">'+
                    '<input type="text" class="form-control mt-1" name="slides['+index+'][url]" placeholder="url" value="{{$slide->url}}">'+
                    '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                        '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                    '</button>'+
                '</div>');
            index++;
    @endforeach

    $('#containerTop').on('click', "#delete", function (){
                $(this).parent().children("#onDelete").attr('value', 'true')
                $(this).parent().attr('style','display: none;')
            });

    $('#containerNews').on('click', "#delete", function (){
        $(this).parent().children("#onDelete").attr('value', 'true')
        $(this).parent().attr('style','display: none;')
            });

    $('#containerTop').on('change','[type="file"]',function(event){
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            $(this).parent().children('img').attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
        }
    })

    $('#containerNews').on('change','[type="file"]',function(event){
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            $(this).parent().children('img').attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
        }
    })
</script>


<style>
/* CSS */
.btn-circle {
    width: 28px;
    height: 28px;
    border-radius: 14px;
    text-align: center;
    padding-left: 0;
    padding-right: 0;
    font-size: 10px;
    background-color: white;
}
</style>


@endsection
