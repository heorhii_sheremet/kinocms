@extends('admin.layout')
@section('content')

<div class="container mt-3">
    <form action="{{(isset($session) && $session->idSession ? route('sessions.update', ['session' => $session->idSession]) : route('films.sessions.store', ['film' => $film->idFilm]))}}" method="POST" enctype = "multipart/form-data" id="sessionsForm">
        @csrf
        @isset($session->idSession)
            @method('PUT')
        @endisset
        <input type="hidden" name="idFilm" value="{{$film->idFilm}}">
        <div class="row mt-5">
            <div class="col-auto">
                <label class="col-form-label">{{__('Film name: ')}}</label>
            </div>
            <div class="col-auto">
                <p class="form-control">{{$film->name}}</p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-1">
                <label class="col-form-label">{{__('Cinema: ')}}</label>
            </div>
            <div class="col-auto">
                <select class="form-select" id="select-cinema">
                    @foreach($cinemas as $cinema)
                        <option value="{{$cinema->idCinema}}" {{isset($session->cinema) && $cinema->idCinema === $session->cinema->idCinema ? 'selected' : ''}}>{{$cinema->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-1">
                <label class="col-form-label">{{__('Hall: ')}}</label>
            </div>
            <div class="col-auto">
                <select class="form-select" name="idHall" disabled id="select-hall">
                    <option value="-1">Hall</option>
                </select>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-1">
                <label class="col-form-label">{{__('Date: ')}}</label>
            </div>
            <div class="col-auto">
                
                <input class="form-control" type="datetime-local" name="date" value="{{isset($session->date) ? date('Y-m-d\TH:i:s', strtotime($session->date)) : ''}}">
            </select>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
            </div>
        </form>
        </div>
    </form>
    @isset($seatsSession)
        <table class="table mt-4">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{__('email')}}</th>
                <th scope="col">{{__('Fullname')}}</th>
                <th scope="col">{{__('Row')}}</th>
                <th scope="col">{{__('Col')}}</th>
            </tr>
            </thead>
            <tbody>
                @foreach($seatsSession as $index => $tiket)
                    <tr>
                        <th scope="row">{{$index + 1}}</th>
                        <td>{{$tiket->user->email}}</td>
                        <td>{{$tiket->user->lastName}} {{$tiket->user->firstName}}</td>
                        <td>{{$tiket->seat->row}}</td>
                        <td>{{$tiket->seat->col}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endisset
</div>

<script>
$('#select-cinema').click(function () {
    $('#select-hall').empty();

    idCinema = $('#select-cinema').val();
    @foreach($cinemas as $cinema)
        if(idCinema === '{{$cinema->idCinema}}') {
            @foreach($cinema->halls as $hall)
                $('#select-hall').append('<option value="{{$hall->idHall}}">Hall {{$hall->number}}</option>')
            @endforeach
        }
    @endforeach
    $('#select-hall').prop('disabled', false);

});

@isset($session)
    $('#select-hall').empty();
    idCinema = $('#select-cinema').val();
    @foreach($cinemas as $cinema)
    if(idCinema === '{{$cinema->idCinema}}') {
        @foreach($cinema->halls as $hall)
            $('#select-hall').append('<option value="{{$hall->idHall}}" {{$hall->idHall === $session->hall->idHall ? 'selected' : ''}}>Hall {{$hall->number}}</option>')
        @endforeach
    }
    @endforeach
    $('#select-hall').prop('disabled', false);
@endisset

</script>

@endsection