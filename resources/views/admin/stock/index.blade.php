@extends('admin.layout')
@section('content')

<p class="text-center mt-3">{{__('List of stock')}}</p>
<div class="row align-items-center">
    <div class="col-3" id="btnWrapper">
        <a href="{{route('stock.create')}}" class="btn btn-outline-success">
            <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
        </a>
    </div>
</div>
<script>
    @foreach ($stock as $stockElement)
        url ="{{$stockElement->logo}}"
        if (url === '') url = '{{asset(config("app.defaultPicture"))}}'
        else url ="{{asset($stockElement->logo)}}"
        $('#btnWrapper').before('<div class="card m-3 p-0" style="width: 16rem; height: 16rem;">'+
            '<img src="'+url+'" class="card-img-top" alt="{{__('Image')}}" style="max-width: 100%; height: 200px">'+
            '<div class="card-body text-center">'+
                '<p class="card-text">{{$stockElement->name}}</p>'+
                '<a href="{{route('stock.edit', ['stock' => $stockElement->idStock])}}" class="stretched-link"></a>'+
            '</div>'+
        '</div>');
    @endforeach
</script>
@endsection
