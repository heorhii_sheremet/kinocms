@extends('admin.layout')
@section('content')

<div class="container mt-3">
    <form action="{{(isset($stock) && $stock->idStock ? route('stock.update', ['stock' => $stock->idStock]) : route('stock.store'))}}" method="POST" enctype = "multipart/form-data" id="stockForm">
        @csrf
        @isset($stock->idStock)
            @method('PUT')
        @endisset
        <div class="row">
            <div class="col-auto ms-auto form-check form-switch form-control-lg text-end">
                <input class="form-check-input" type="checkbox" name="state" role="switch" {{isset($stock->state) && $stock->state == 1 ? 'checked' : ''}}>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Stock name')}}</label>
            </div>
            <div class="col-auto">
                <input type="text" name="name" class="form-control" value="{{isset($stock->name) ? $stock->name : ''}}" required>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Description')}}</label>
            </div>
            <div class="col-auto">
                <textarea class="form-control" name="description" cols="70" rows="10">{{isset($stock->description) ? $stock->description : ''}}</textarea>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Published date')}}</label>
            </div>
            <div class="col-auto">
                <input type="date" name="publishedDate" class="form-control" value="{{isset($stock->publishedDate) ? date('Y-m-d', strtotime($stock->publishedDate)) : ''}}">
            </div>
        </div>
        <div class="row align-items-center mb-3" id="logo">
            <div class="col-auto">
                <label class="col-form-label">{{__('Logo')}}</label>
            </div>
            <div class="col-auto">
                <img class="img-fluid rounded" src="{{(isset($stock->logo) && $stock->logo) ? asset("$stock->logo") : asset(config("app.defaultPicture"))}}" style="max-width: 300px; height: 200px" alt="{{ __('logo') }}">
            </div>
            <div class="col-auto">
                <div class="row">
                    <div class="col-auto">
                        <input class="form-control" type="file" name="logo" id="logoImg">
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-danger" type="button" id="deleteLogo">{{__('Delete')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Video link')}}</label>
            </div>
            <div class="col-auto">
                <input type="text" name="videoLink" class="form-control" value="{{isset($stock->videoLink) ? $stock->videoLink : ''}}">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('Video link')}}</label>
            </div>
            <div class="col-auto">
                <select class="form-select" name="cinemas[]" multiple>
                    @foreach($cinemas as $cinema)
                        <option value="{{$cinema->idCinema}}"
                            @isset($stock->cinemas)
                                {{$stock->cinemas->contains(function ($stockCinema, $key) use($cinema) {
                                    return $stockCinema->idCinema === $cinema->idCinema;
                                }) ? 'selected' : ''}}
                            @endisset
                        > {{$cinema->name}} </option>
                    @endforeach
                  </select>
            </div>
        </div>
        <p>{{__('Picture gallery')}}</p>
            <div class="col-auto">
                <p>{{__('Size')}}: 1000x190</p>
            </div>
        <div class="row align-items-center mb-3" id='gallery'>
            <div class="col-3 mt-2" id="btnWrapper">
                <button type="button" class="btn btn-outline-success" id="btn">
                    <img src="{{asset('default-images/plus-lg.svg')}}" width="30" height="30" alt="{{ __('Add image') }}">
                </button>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <label class="col-form-label">{{__('SEO block')}}</label>
            </div>
            <div class="col-9">
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('URL:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="SEOblock[url]" class="form-control" value="{{ isset($stock->SEOBlock->url) ? $stock->SEOBlock->url : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Title:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="SEOblock[title]" class="form-control" value="{{ isset($stock->SEOBlock->title) ? $stock->SEOBlock->title : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Keywords:')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="SEOblock[keywords]" class="form-control" value="{{ isset($stock->SEOBlock->keywords) ? $stock->SEOBlock->keywords : '' }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Description:')}}</label>
                    </div>
                    <div class="col">
                        <textarea class="form-control" name="SEOblock[description]" rows="3">{{ isset($stock->SEOBlock->description) ? $stock->SEOBlock->description : '' }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
            </div>
        </form>
            @isset($stock->idStock)
                <div class="col-auto">
                    <form action="{{route('stock.destroy', ['stock' => $stock->idStock])}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input class="btn btn-danger mt -5" type="submit" value="{{__('Delete Stock')}}"/>
                    </form>
                </div>
            @endisset
        </div>
</div>
<script>
    $('#deleteLogo').on('click', function() {
        $('#stockForm').append('<input type="hidden" name="deleteLogo">')
        $("#logo").find("img").attr("src","{{asset(config("app.defaultPicture"))}}");
        $('#logoImg').val('')
    })
    index = 0;
    $('#btn').on('click', function() {
        $('#btnWrapper').before('<div class="col-auto text-center mt-5" style="position: relative; max-width: 337px;">'+
                    '<img src="{{asset(config("app.defaultPicture"))}}" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                    '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                    '<input type="file" class="form-control mt-1" name="slides['+index+'][img]" required>'+
                    '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                        '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                    '</button>'+
                '</div>');
            index++;
    })
    @isset($stock)
        @foreach ($stock->pictures as $slide)
            $('#btnWrapper').before('<div class="col-auto text-center mt-5" style="position: relative; max-width: 337px;">'+
                '<input type="hidden" name="slides['+index+'][id]" value={{$slide->idPicture}}>'+
                '<img src="{{(isset($slide->img) && $slide->img) ? asset("$slide->img") : asset(config("app.defaultPicture"))}}" alt="{{__('Image')}}" style="max-width: 100%; height: 200px" class="img-fluid">'+
                '<input type="hidden" name="slides['+index+'][onDelete]" value="false" id="onDelete">'+
                '<input type="file" class="form-control mt-1" name="slides['+index+'][img]">'+
                '<button type="button" class="btn btn-circle" style="position: absolute; top: -20px; right: 0px" id="delete">'+
                    '<img src="{{asset('default-images/x-circle.svg')}}" alt="x" width="20" height="20">'+
                '</button>'+
            '</div>');
            index++;
        @endforeach
    @endisset
    $('#gallery').on('click', "#delete", function (){
                $(this).parent().children("#onDelete").attr('value', 'true')
                $(this).parent().attr('style','display: none;')
    });

    $('#logoImg').on('change', function(event) {
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            console.log($(this).closest("#logo").find("img"))
            $(this).closest("#logo").find("img").attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
            $("input[name = 'deleteLogo']").remove()
        }
    })

    $('#gallery').on('change','[type="file"]',function(event){
        var files = event.target.files, file;
        if (files && files.length > 0) {
                    // Получаем загруженный в данный момент файл
            file = files [0]; // Действие проверки размера файла
            if(file.size > 1024 * 1024 * 2) {
                alert ('Размер картинки не может превышать 2 МБ!');
                return false;
            }
            // Получить инструмент URL окна
            var URL = window.URL || window.webkitURL;
            // Генерировать целевой URL через файл, чтобы получить реальный путь
            var imgURL = URL.createObjectURL(file);
            // Используйте attr, чтобы изменить атрибут src img на полученный url
            $(this).parent().children('img').attr("src",imgURL);
            // Используйте следующее предложение, чтобы освободить сервопривод для этого URL-адреса в памяти, и URL-адрес станет недействительным после запуска
            // URL.revokeObjectURL(imgURL);
        }
    })
    
</script>
<style>
    /* CSS */
    .btn-circle {
        width: 28px;
        height: 28px;
        border-radius: 14px;
        text-align: center;
        padding-left: 0;
        padding-right: 0;
        font-size: 10px;
        background-color: white;
    }
    </style>
@endsection