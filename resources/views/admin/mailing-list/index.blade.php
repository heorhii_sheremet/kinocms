@extends('admin.layout')
@section('content')
<p class="text-center mt-3 display-6">{{__('E-mail')}}</p>
<div class="row">
    <form action="#" method="POST" enctype = "multipart/form-data" id='mailingForm'>
        @csrf
        <div class="form-check form-switch ms-1">
            <input class="form-check-input" type="checkbox" name="sendEvryone" role="switch" id="switch">
            <label class="form-check-label" for="switch">{{__('send to everyone')}}</label>
        </div>
        <div class="col-auto" id="usersList">
            <table class="table mt-3">
                <thead>
                  <tr>
                    <th scope="col">{{__('ID')}}</th>
                    <th scope="col">{{__('Date of registration')}}</th>
                    <th scope="col">{{__('Date of Birth')}}</th>
                    <th scope="col">{{__('Email')}}</th>
                    <th scope="col">{{__('Phone')}}</th>
                    <th scope="col">{{__('Full name')}}</th>
                    <th scope="col">{{__('Nick name')}}</th>
                    <th scope="col">{{__('City')}}</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                @foreach($users as $index => $user)
                  <tr>
                    <th scope="row">{{$index + 1}}</th>
                    <td>{{$user->created_at}}</td>
                    <td>{{$user->birthday}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    <td>{{$user->firstName}} {{$user->lastName}}</td>
                    <td>{{$user->nickName}}</td>
                    <td>{{$user->city}}</td>
                    <td>
                        <input class="form-check-input checkbox" name="users[{{$user->idUser}}]" type="checkbox" value="{{$user->idUser}}" id="{{$user->idUser}}">
                        <label class="form-check-label" for="{{$user->idUser}}">
                    </td>
                  </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-12 ms-1 mt-2">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="temlateType" value="downloadTemplate" id="downloadTemplate" checked>
                <label class="form-check-label" for="downloadTemplate">
                    {{('Download a temlate')}}
                </label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="temlateType" value="chooseTemplate" id="chooseTemplate">
                <label class="form-check-label" for="chooseTemplate">
                    {{__('Choose a template')}}
                </label>
            </div>
        </div>
        <div class="col-3 mt-2 ms-1" id="inputTemplate">
            <input type="file" name="downloadedTemplate" class="form-control" id = "templateFile">
        </div>
        <div class="col-3 mt-2 ms-1" id="listTemplates">
            <div class="text">{{__('List of recently downloaded templates')}}</div>
            @foreach($templates as $template)
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="template" value="{{$template->idLetterTemplate}}" id="{{$template->idLetterTemplate}}">
                <label class="form-check-label" for="{{$template->idLetterTemplate}}">
                    {{$template->file}}
                </label>
            </div>
            @endforeach
        </div>
        <div class="col-12" id='processing'>
        </div>
        <input type="submit" value="Send" class="btn btn-primary mt-2 ms-1" id="send">
    </form>
</div>
<script>
    $('#listTemplates').css("display", "none")
    total = 0;
    $('#switch').change(function() {
        if($('#switch').prop('checked')){
            $('#usersList').css("display", "none")
            total = "{{$users->count()}}";
            console.log(total);
        }
        else {
            $('#usersList').css("display", "block")
            total = 0;
            $('input:checkbox:checked').each(function(){
                total += 1;
            });
            console.log(total);
        }
    })
    $('#downloadTemplate').click(function() {
        $('#inputTemplate').css("display", "block")
        $('#listTemplates').css("display", "none")
    })
    $('#chooseTemplate').click(function() {
        $('#inputTemplate').css("display", "none")
        $('#listTemplates').css("display", "block")
    })
    $('.checkbox').click( function() {
        total = 0;
        $('input:checkbox:checked').each(function(){
            total += 1;
	    });
        console.log(total);
    })

    $(function() {

        $("#mailingForm").submit(function(e) {
            e.preventDefault();    
            var formData = new FormData(this);
            $.ajax({
                url: '{{ route( 'mailing.send' )}}',
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if(data['error']) {
                        var str = '<div class="alert alert-danger mt-2" role="alert">'+ data['error'] +'!</div>'
                        $('#processing').append(str);
                        return
                    }   
                    var interval = 1000;  // 1000 = 1 second, 3000 = 3 seconds
                    function getQueueCount() {

                        ajaxData = '';
                        $.ajax({
                                type: 'GET',
                                url: '{{ route( 'mailing.getQueueCount' )}}',
                                headers: {
                                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function (data) {
                                        ajaxData = data
                                        passed = (total - data['left'])
                                        progress = passed / total * 100
                                        console.log(progress)
                                        $('#progress').remove()
                                        $('#processing').append('<div class="progress ms-5 mt-2 me-5" id="progress">'+
                                            '<div class="progress-bar" role="progressbar" style="width: '+ progress +'%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">'+ progress +'%</div>'+
                                        '</div>')
                                        console.log(data['left'])
                                },
                                complete: function () {
                                        // Schedule the next
                                        console.log(ajaxData['left'])
                                        if(ajaxData['left'] > 0) setTimeout(getQueueCount, interval);
                                        else {
                                            var str = '<div class="alert alert-success mt-2" role="alert"> Messages sent !</div>'
                                            $('#processing').append(str);
                                        }   
                                }
                        });
                    }
                    setTimeout(getQueueCount, interval);
                },
                error: function (msg) {
                    alert('Ошибка');
                }
            });

        });

    })
</script>
@endsection
