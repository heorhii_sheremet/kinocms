@extends('admin.layout')
@section('content')

<p class="text-center mt-3">{{__('Users')}}</p>
<div class="row align-items-center">
    <table class="table m-e-5">
        <thead>
          <tr>
            <th scope="col">{{__('ID')}}</th>
            <th scope="col">{{__('Date of registration')}}</th>
            <th scope="col">{{__('Date of Birth')}}</th>
            <th scope="col">{{__('Email')}}</th>
            <th scope="col">{{__('Phone')}}</th>
            <th scope="col">{{__('Full name')}}</th>
            <th scope="col">{{__('Nick name')}}</th>
            <th scope="col">{{__('City')}}</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
          <tr>
            <th scope="row">{{$user->idUser}}</th>
            <td>{{$user->created_at}}</td>
            <td>{{$user->birthday}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->phone}}</td>
            <td>{{$user->firstName}} {{$user->lastName}}</td>
            <td>{{$user->nickName}}</td>
            <td>{{$user->city}}</td>
            <td class="col-2">
                <div class="button-group mt-0">
                    <form action="{{route('users.destroy', ['user' => $user->idUser])}}" method="post">
                      <a class="btn btn-primary" href="{{route('users.edit', ['user' => $user->idUser])}}">{{__('edit')}}</a>
                      @csrf
                      @method('DELETE')
                      <input class="btn btn-danger" type="submit" value="{{__('delete')}}"/>
                    </form>
                </div>
            </td>
          </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
