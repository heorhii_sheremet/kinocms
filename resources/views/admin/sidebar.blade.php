<div class="d-flex flex-column flex-shrink-0 p-3 bg-light">
    <ul class="nav nav-pills flex-column mb-auto">
      <li class="nav-item">
        <a href="/admin" class="nav-link link-dark" aria-current="page" id="admin">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"></use></svg>
          {{ __('Statistics') }}
        </a>
      </li>
      <li>
        <a href="/admin/banners" class="nav-link  link-dark" id="banners">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg>
          {{ __('Banners') }}
        </a>
      </li>
      <li>
        <a href="/admin/films" class="nav-link link-dark" id="films">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#table"></use></svg>
          {{ __('Films') }}
        </a>
      </li>
      <li>
        <a href="/admin/cinemas" class="nav-link link-dark" id="cinemas">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#table"></use></svg>
          {{ __('Cinema') }}
        </a>
      </li>
      <li>
        <a href="/admin/news" class="nav-link link-dark" id="news">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#grid"></use></svg>
          {{ __('News') }}
        </a>
      </li>
      <li>
        <a href="/admin/stock" class="nav-link link-dark" id ="stock">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg>
          {{ __('Stocks') }}
        </a>
      </li>
      <li>
        <a href="/admin/pages" class="nav-link link-dark" id ="pages">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#grid"></use></svg>
          {{ __('Pages') }}
        </a>
      </li>
      <li>
        <a href="/admin/users" class="nav-link link-dark" id ="users">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#grid"></use></svg>
          {{ __('Users') }}
        </a>
      </li>
      <li>
        <a href="/admin/mailing" class="nav-link link-dark" id="mailing">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#grid"></use></svg>
          {{ __('Mailing list') }}
        </a>
      </li>
    </ul>
  </div>
  <script>
      let slug = url => new URL(url).pathname.match(/[^\/]+/g)
      className = slug(document.location)[1] ? slug(document.location)[1] : "admin"
      let elem = document.getElementById(className);
      try {
      elem.classList.add("active");
      }
      catch (e) {

      }
  </script>