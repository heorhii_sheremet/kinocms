<div class="container border-top border-dark mt-5">
    <div class="row mt-3">
        <div class="col-5 mt-3">
            <h5 class="text">
                    {{__('Mobile apps')}}
            </h5>
            <div class="row">  
                <div class="col-4">
                    <img src="{{asset('default-images/appStore.png')}}" class="img-fluid" alt="{{__('Image')}}">
                </div>
                <div class="col-4">
                    <img src="{{asset('default-images/GooglePlay.png')}}" class="img-fluid" alt="{{__('Image')}}">
                </div>
            </div>
            <div class="col text mt-4">
                {{__('Website development')}}: AVADA-MEDIA
            </div>
        </div>
        <div class="col-2">
            <div class="row">
                <div class="col-12 ms-2">
                    <a class="p-2 text-dark text-decoration-none" href="{{route('poster')}}"><h5 class="text">{{__('Poster')}}</h5></a>
                </div>
                <div class="col-12 mb-2">
                    <a class="p-2 text-dark text-decoration-none" href="{{route('timetable')}}">{{__('Timetable')}}</a>
                </div>
                <div class="col-12 mb-2">
                    <a class="p-2 text-dark text-decoration-none" href="{{route('poster', ['soon' => true])}}">{{__('Soon')}}</a>
                </div>
                <div class="col-12 mb-2">
                    <a class="p-2 text-dark text-decoration-none" href="{{route('cinemas')}}">{{__('Cinemas')}}</a>
                </div>
                <div class="col-12 mb-2">
                    <a class="p-2 text-dark text-decoration-none" href="{{route('stock')}}">{{__('Stock')}}</a>
                </div>
            </div>
        </div>
        <div class="col-2 mt-4">
            <div class="row">
                <div class="col-12 ms-2 mb-4">
                    <h5 class="text-dark text-decoration-none">{{__('About cinema')}}</h5>
                </div>
                <div class="col-12 mb-2">
                    <a class="p-2 text-dark text-decoration-none" href="{{route('news')}}">{{__('News')}}</a>
                </div>
                <div class="col-12 mb-2">
                    <a class="p-2 text-dark text-decoration-none" href="{{route('contacts')}}">{{__('Contacts')}}</a>
                </div>
                <div class="col-12 mb-2">
                    <a class="p-2 text-dark text-decoration-none" href="{{route('mobile-app')}}">{{__('Mobile App')}}</a>
                </div>
                @foreach($navPages as $page)
                    @if($page->state == 1)
                        <div class="col-12 mb-2">
                            <a class="p-2 text-dark text-decoration-none" href="{{route('page',['page' => $page->idPage])}}">{{$page->name}}</a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="col-3 text-center mt-5">
            {{__('Icons')}}
        </div>
    </div>
    <div class="col-12 text-center h6 mt-4">
        &#169 KinoCMS 2022, All right reserved
    </div>
</div>