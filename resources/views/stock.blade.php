@extends('layout')
@section('content')
<div class="row">
    <div class="col-9">
        <div class="container">
            <div class="text-center display-6">
                {{__('Our stock')}}
            </div>
            <div class="row align-items-start bg-secondary">
                @foreach($stock as $OneStock)
                <div class="col-auto mb-4 mt-2">
                    <div class="card border-secondary" style="width: 23rem; height: 30rem;">
                        <img src="{{ $OneStock->logo !== '' ? asset("$OneStock->logo") : asset(config("app.defaultPicture")) }}" class="card-img-top" alt="Image Stock" style="width: 23rem; height: 15rem;">
                        <div class="card-body text-start bg-secondary">
                            <a href="{{route('stock.show', ['stock' => $OneStock->idStock])}}">{{$OneStock->name}}</a><br>
                            <span class="text-white bg-dark rounded-pill p-1">{{date('d.m.Y', strtotime($OneStock->publishedDate))}}</span>
                            @foreach($OneStock->cinemas as $cinema)
                                <span class="text-white bg-info m-1 rounded-pill p-1">{{$cinema->name}}</span>
                            @endforeach
                            <p class="card-text mt-1">{{$OneStock->description}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-3 bg-lght pt-5">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id facilis sequi incidunt a molestiae, iusto minima. Autem quod odio nesciunt, fugit dolores alias vitae at obcaecati, eum sed voluptate ex!
    </div>

</div>
@endsection