@extends('layout')
@section('content')
<div class="text-center">
    <img src="{{asset($hall->topBanner)}}" alt="{{__('Image')}}" style="width: 98%; max-height: 600px;">
</div>
<div class="row mt-5 ms-1">
    <div class="col-auto" style="width: 20%">
        <div class="row">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolorum a saepe, quae ut qui ex et facilis quidem suscipit rem minima atque laudantium reprehenderit magni adipisci accusantium tempora est praesentium?
        </div>
        <div class="col-auto mt-3 ms-2">
            <div class="text mb-3 ps-1">
                {{__('Watch today')}}
            </div>
            <ul class="list-group">
                @foreach($sessions as $index => $session)
                    <li class="list-group-item d-grid"><a class="btn text-start" href="{{route('booking', ['idSession' => $session->idSession])}}">{{__('Session')}} {{$index+1}}</a></li>
                @endforeach
              </ul>
        </div>
        <div class="col-12 d-grid ps-2 mt-4">
            <a href="{{route('timetable', ['hall' => $hall->idHall])}}" class="btn btn-success">{{__('Timetable')}}</a>
        </div>
    </div>
    <div class="col ms-5 me-5">
        <div class="row">
            <div class="col display-6 text-center">
                {{__('Hall')}} {{$hall->number}}
            </div>
        </div>
        <div class="row mt-4">
            {{$hall->description}}
        </div>
        <div class="row mt-4">
            <div class="text-center display-6 mb-4">
                {{__('Hall map')}}
            </div>
            <div class="text-start bg-light text-light">
                <div class="row" style="min-width: 1200px;">
                    <div class="col-12 text-center" id="wrapper">
                        <div class="row">
                            <div class="col-1"> </div>
                            <div class="col mt-5 mb-5">
                                <h6 class="text-secondary">
                                    <hr class="my-1">
                                    {{__('Screen')}}
                                </h6>
                            </div>
                        </div>
                        @foreach($seats as $index => $row)
                            <div class="row" style="white-space: nowrap !important;">
                                <div class="col-1" style="display: inline-block !important;">
                                    <button type="button" class="btn btn-white mt-1" disabled>{{__('Row')}} {{$index}}</button>
                                </div>
                                <div class="col">
                                    <div class="hall">
                                        @foreach($row as $col)
                                            <div class="seat d-grid" style="display: inline-block !important;">
                                                <button type="button" class="btn btn-warning seat" id="{{$col->idSeat}}" style="width: 50px" disabled>{{$col->col}}</button>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="text-center display-6 mb-4">
                {{__('Photo gallery')}}
            </div>
        </div>
        <div class="row">
            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel" style=" width: 100%; height: 700px; overflow: hidden;">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{isset($hall->topBanner) ? asset($hall->topBanner) : asset(config("app.defaultPicture"))}}" class="d-block w-100" alt="...">
                    </div>
                    @foreach($hall->pictures as $picture)
                    <div class="carousel-item">
                        <img src="{{isset($picture->img) ? asset($picture->img) : asset(config("app.defaultPicture"))}}" class="d-block w-100" alt="...">
                    </div>
                    @endforeach
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    </div>
</div>
@endsection