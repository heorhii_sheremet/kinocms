@extends('layout')
@section('content')
<div class="row">
    <img src="{{asset($page->logo)}}" height="600" width="auto" alt="{{__('Image')}}">
    <div class="col-9">
        <div class="container-fluent ms-1 ps-3 pb-4">
            <div class="display-6 text-center">{{$page->name}}</div>
            <div class="row mt-5">
                <div class="col-12 text-break pe-4">
                    <span>{{$page->description}}</span>
                </div>
            </div>
            <div class="row mt-2 pe-2">
                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel" style=" width: 100%; height: 700px; overflow: hidden;">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{isset($page->logo) ? asset($page->logo) : asset(config("app.defaultPicture"))}}" class="d-block w-100" alt="...">
                        </div>
                        @foreach($page->pictures as $picture)
                        <div class="carousel-item">
                            <img src="{{isset($picture->img) ? asset($picture->img) : asset(config("app.defaultPicture"))}}" class="d-block w-100" alt="...">
                        </div>
                        @endforeach
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-3 bg-lght pt-5">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id facilis sequi incidunt a molestiae, iusto minima. Autem quod odio nesciunt, fugit dolores alias vitae at obcaecati, eum sed voluptate ex!
    </div>

</div>
@endsection