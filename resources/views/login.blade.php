@extends('layout')
@section('content')
    <div class="container w-50 pt-5">
        <div class="text-center display-6 mb-5">
            {{__('Log in')}}
        </div>
        @if($errors->first('error_auth'))
            <label class="form-label text-danger pb-2">
                {{$errors->first('error_auth')}}
            </label>
        @endif
        <form action="{{route('login.auth')}}" method="POST">
            @csrf
            <div class="mb-3">
              <label for="InputEmail1" class="form-label">{{__('Email address')}}</label>
              <input type="email" name="email" class="form-control" id="InputEmail1" aria-describedby="emailHelp" required>
            </div>
            <div class="mb-3">
              <label for="password" class="form-label">{{__('Password')}}</label>
              <input type="password" name="password" class="form-control" id="password" required>
            </div>
            <button type="submit" class="btn btn-primary" id="submit">{{__('Submit')}}</button>
            <a href="{{route('registration')}}" class="link ms-5 text-decoration-none">{{__('Registration')}}</a>
          </form>
    </div>
@endsection