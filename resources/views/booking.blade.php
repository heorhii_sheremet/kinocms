@extends('layout')
@section('content')
<div class="row m-3">
    <div class="col-auto">
        <div class="row">
            <div class="col-auto">
                <img src="{{isset($session->film->mainPicture) ? asset($session->film->mainPicture) : asset(config("app.defaultPicture"))}}" width="500" alt="Logo">
            </div>
        </div>
        <div class="row bg-light mt-3">
            <div class="col text-center" style="width: 500px">
               Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente dolores repudiandae, modi sint nobis beatae. Autem, atque exercitationem magni harum corporis nulla eum quod molestiae, voluptatem et dolorem, doloremque voluptates.
            </div>
        </div>
    </div>
    <div class="col">
        <div class="row">
            <div class="col-auto bg-danger text-white display-6">
                {{$session->film->name}}
            </div>
        </div>
        <div class="row">
            <div class="col-auto text-secondary mt-2">
                {{date('d M, H:i,', strtotime($session->date))}} {{__('Hall')}}№ {{$session->hall->number}}
            </div>
        </div>
        <div class="row mt-3 ps-5 pe-5">
            <div class="col-3">
                {{__('Price')}}: <span class="bg-warning text-white p-1">{{$session->film->price}}</span>
            </div>
            <div class="col-3">
                {{__('Booked')}}:
                <button type="button" class="btn btn-secondary seat mb-1" style="width: 50px" disabled>
                    <img src="{{asset('default-images/person-fill.svg')}}" width="20" alt="booked">
                </button>
            </div>
            <div class="col-6">
                {{__('Your order')}}:
                <span class="bg-warning p-2">
                    <span class="bg-white p-1"> {{__('Tickets')}}: <span class="text-danger p-1" id="tickets">0</span>  {{__('Sum')}}: <span class="text-danger p-1" id="sum">0</span></span>
                </span >
            </div>
        </div>
        <div class="row" style="min-width: 1200px;">
            <div class="col-12 text-center" id="wrapper">
                <div class="row">
                    <div class="col-1"> </div>
                    <div class="col mt-5 mb-5">
                        <h6 class="text-secondary">
                            <hr class="my-1">
                            {{__('Screen')}}
                        </h6>
                    </div>
                </div>
                @foreach($seats as $index => $row)
                    <div class="row" style="white-space: nowrap !important;">
                        <div class="col-1" style="display: inline-block !important;">
                            <button type="button" class="btn btn-white mt-1" disabled>{{__('Row')}} {{$index}}</button>
                        </div>
                        <div class="col">
                            <div class="hall">
                                @foreach($row as $col)
                                @if($session->seatsSession->where('idSeat', $col->idSeat)->count() > 0)
                                <button type="button" class="btn btn-secondary seat" id="{{$col->idSeat}}" style="width: 50px" disabled>
                                    <img src="{{asset('default-images/person-fill.svg')}}" width="20"  alt="booked">
                                </button>
                                @else
                                    <div class="seat d-grid" style="display: inline-block !important;">
                                        <button type="button" class="btn btn-warning seat" id="{{$col->idSeat}}" style="width: 50px">{{$col->col}}</button>
                                    </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row mt-3 justify-content-left">
            <div class="col-auto offset-3">
                <form action="{{route('booking.save')}}" method="POST" id="booking">
                    @csrf
                    <input type="hidden" name='idSession' value="{{$session->idSession}}">
                    <input type="hidden" name='status' value="booked">
                    <button type="submit" class="btn btn-primary">{{__('Book')}}</button>
                </form>
            </div>
            <div class="col-auto">
                <form action="#" method="POST" id="booking">
                    @csrf
                    <button type="submit" class="btn btn-primary" disabled>{{__('Buy')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    sumTickets = 0
    sumPrice = 0
    $('.seat').click( function() {
        numberTicket = $(this).attr('id');
        if($(this).hasClass('btn-warning')) {

            $(this).removeClass('btn-warning')
            $(this).addClass('btn-success')
            
            
            sumTickets += 1
            sumPrice += {{$session->film->price}}
            $('#tickets').text(sumTickets)
            $('#sum').text(sumPrice)

            $('#booking').append('<input type="hidden" name="tickets[' +numberTicket+ ']" value="' +numberTicket+ '">')
        }
        else if ($(this).hasClass('btn-success')) {

            $(this).removeClass('btn-success')
            $(this).addClass('btn-warning')
            
            sumTickets -= 1
            sumPrice -= {{$session->film->price}}
            $('#tickets').text(sumTickets)
            $('#sum').text(sumPrice)

            $('#booking').children('input[name="tickets['+numberTicket+']"]').remove()
        } 
    })
</script>
@endsection