@extends('layout')
@section('content')
    <div class="container w-50 pt-5">
        <div class="text-center display-6 mb-5">
            {{__('Registration')}}
        </div>
        @if($errors->first('email_alredy_is'))
            <label class="form-label text-danger pb-2">
                {{$errors->first('email_alredy_is')}}
            </label>
        @endif
        <form action="{{route('userSave')}}" method="POST">
            @csrf
            <div class="mb-3">
              <label for="InputEmail1" class="form-label">{{__('Email address')}}</label>
              <input type="email" name="email" class="form-control" id="InputEmail1" aria-describedby="emailHelp" required>
            </div>
            <div class="mb-3">
              <label for="password" class="form-label">{{__('Password')}}</label>
              <input type="password" name="password" class="form-control" id="password" required>
            </div>
            <div class="mb-3">
                <label for="repeatPassword" class="form-label">{{__('Repeat password')}}</label>
                <input type="password" class="form-control" id="repeatPassword" required>
            </div>
            <div class="mb-3">
                <label for="nickname" class="form-label">{{__('Nickname')}}</label>
                <input type="text" name="nickName" class="form-control" id="nickname" required>
            </div>
            <button type="submit" class="btn btn-primary" id="submit">{{__('Submit')}}</button>
          </form>
    </div>
    <script>
        $('#repeatPassword, #password').on('change', function() {
            password = $('#password').val();
            repeatPassword = $('#repeatPassword').val();
            console.log(password)
            if(password == '' || password === repeatPassword){
                $('#submit').prop('disabled',false);
                $('#repeatPassword').removeClass('bg-danger');
            }
            else {
                $('#submit').prop('disabled',true);
                $('#repeatPassword').addClass('bg-danger');
            }
            
        })
        </script>
@endsection