@extends('layout')
@section('content')
<div class="container mt-5">
    <div class="row bg-light pt-3 ps-3">
        <div class="col-auto">
            <div class="row">
                <p class="col-auto pt-2">
                    {{__('Film type')}}
                </p>
               
                <div class="col-auto">
                    <select class="form-select mb-3" id="type">
                        <option value="-1" >{{__('All')}}</option>
                        <option value="2d" {{ isset($filters['type']) ? ($filters['type'] === '2d' ? 'selected' : '') : '' }}>{{__('2D')}}</option>
                        <option value="3d" {{ isset($filters['type']) ? ($filters['type'] === '3d' ? 'selected' : '') : '' }}>{{__('3D')}}</option>
                        <option value="imax" {{ isset($filters['type']) ? ($filters['type'] === 'imax' ? 'selected' : '') : '' }}>{{__('Imax')}}</option>
                    </select>
                </div>
            </div>
        </div>
    <div class="col-auto">
        <div class="row">
                <p class="col-auto pt-2">
                    {{__('Cinema')}}
                </p>
                <div class="col-auto">
                    <select class="form-select mb-3" id='cinema'>
                        <option value="-1">{{__('All')}}</option>
                        @foreach($cinemas as $cinema)
                            <option value="{{$cinema->idCinema}}" {{ isset($filters['cinema']) ? ($filters['cinema'] == $cinema->idCinema ? 'selected' : '') : '' }}>{{$cinema->name}}</option>
                        @endforeach
                    </select>
                </div>
        </div>
        </div>
        <div class="col-auto">
            <div class="row">
                <p class="col-auto pt-2">
                    {{__('Film')}}
                </p>
                <div class="col-auto">
                    <select class="form-select mb-3" id='film'>
                        <option value="-1">{{__('All')}}</option>
                        @foreach($films as $film)
                            <option value="{{$film->idFilm}}" {{ isset($filters['film']) ? ($filters['film'] == $film->idFilm ? 'selected' : '') : '' }} >{{$film->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-auto">
            <div class="row">
                <p class="col-auto pt-2">
                    {{__('Hall')}}
                </p>
                <div class="col-auto">
                    <select class="form-select mb-3" id='hall' disabled>
                        <option value="-1">{{__('Select cinema')}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-auto">
            <div class="row">
                <p class="col-auto pt-2">
                    {{__('Date')}}
                </p>
                <div class="col-auto">
                    <input type="date" class="form-control" id='date' value="{{ isset($filters['date']) ? date('Y-m-d', strtotime($filters['date'])) : '' }}">
                </div>
            </div>
        </div>
        <div class="col-auto">
            <a href="{{route('timetable')}}" class="btn btn-primary" id="apply">{{__('Apply')}}</a>
        </div>
    </div>

    @foreach($sessionsByDate as $date => $sessionSet)
        <div class="row bg-light p-3 mt-3">
            <p class="display-6">{{date('d M, D', strtotime($date))}}</p>
            <table class="table table-dark table-striped">
                <thead>
                <tr>
                    <th scope="col">{{__('Time')}}</th>
                    <th scope="col">{{__('Film')}}</th>
                    <th scope="col">{{__('Hall')}}</th>
                    <th scope="col">{{__('Price')}}</th>
                </tr>
                </thead>
                @foreach($sessionSet as $session)
                    <tbody>
                        <tr>
                        <th>{{date('H:i', strtotime($session->date))}}</th>
                        <td> <a href="{{route('booking', ['idSession' => $session->idSession])}}" class="text-white"> {{$films->find($session->idFilm)->name}} </a></td>
                        <td>{{$session->hall->number}}</td>
                        <td>{{$films->find($session->idFilm)->price}}</td>
                        </tr>
                    </tbody>
                @endforeach
            </table>
        </div>
    @endforeach
</div>
<script>
    $('#cinema').click(function () {
        $('#hall').empty();

        idCinema = $('#cinema').val();
        if(idCinema == -1) {
            $('#hall').append('<option value="-1">{{__('Select cinema')}}</option>')
            $('#hall').prop('disabled', true);
        }
        else {
            $('#hall').append('<option value="-1">{{__('All')}}</option>')
            @foreach($cinemas as $cinema)
                if(idCinema === '{{$cinema->idCinema}}') {
                    @foreach($cinema->halls as $hall)
                        $('#hall').append('<option value="{{$hall->idHall}}">Hall {{$hall->number}}</option>')
                    @endforeach
                }
            @endforeach
            $('#hall').prop('disabled', false);
        }

    });

    $('#apply').click( function() {
        filter = '?'
        console.log($('#type').val())
        if ($('#type').val() != -1) {
            filter += 'type=' + $('#type').val() +'&'
        }
        if ($('#cinema').val() != -1) {
            filter += 'cinema=' + $('#cinema').val() +'&'
        }
        if ($('#film').val() != -1) {
            filter += 'film=' + $('#film').val() +'&'
        }
        if ($('#hall').val() != -1) {
            filter += 'hall=' + $('#hall').val() +'&'
        }
        if ($('#date').val() != '') {
            filter += 'date=' + $('#date').val() +'&'
        }
        filter = filter.substring(0, filter.length - 1);
        $('#apply').attr('href', "{{route('timetable')}}"+filter);
    })

    @isset($filters['cinema'])
        $(document).ready(function() {
            $('#hall').empty();
            console.log('here')
            idCinema = "{{$filters['cinema']}}"
            
            $('#hall').append('<option value="-1">{{__('All')}}</option>')
            @foreach($cinemas as $cinema)
                if(idCinema === '{{$cinema->idCinema}}') {
                    @foreach($cinema->halls as $hall)
                        $('#hall').append('<option value="{{$hall->idHall}}" {{ isset($filters['hall']) ? ($filters['hall'] == $hall->idHall ? 'selected' : '') : '' }} >Hall {{$hall->number}}</option>')
                    @endforeach
                }
            @endforeach
            $('#hall').prop('disabled', false);
        
        });
    @endisset
    
    @if(! isset($filters['cinema']))
        @isset($filters['hall'])
            @foreach($cinemas as $cinema)
                @foreach($cinema->halls as $hall)
                    @if($hall->idHall == $filters['hall'])
                        $('#hall').append('<option value="{{$filters['hall']}}" selected>Hall {{$hall->number}}</option>')
                        $('#cinema').find('option[value = "{{$cinema->idCinema}}"]').prop('selected', true)
                    @endif
                    @endforeach
                @endforeach
        @endisset
    @endif
</script>
@endsection