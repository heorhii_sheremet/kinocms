@extends('layout')
@section('content')
<div class="container mt-3">
    <div class="row">
        <div class="text-center offset-2 col-8 display-6 mb-5">
            {{__('Personal Area')}}
        </div>
        <div class="col-2 text-end">
            <a href="{{route('logout')}}" class="btn btn-danger">{{__('Logout')}}</a>
        </div>
    </div>
    <form action="{{route('account.save')}}" method="POST" enctype = "multipart/form-data" id="userForm">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-6">
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('First Name')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="firstName" class="form-control" value="{{ Auth::user()->firstName }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Last Name')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="lastName" class="form-control" value="{{ Auth::user()->lastName}}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Nickname')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="nickName" class="form-control" value="{{ Auth::user()->nickName}}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('E-mail')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="email" class="form-control" value="{{ Auth::user()->email }}" disabled>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Password')}}</label>
                    </div>
                    <div class="col">
                        <input type="password" name="password" class="form-control" id="password">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Address')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="address" class="form-control" value="{{ Auth::user()->address }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Card number')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="cardNumber" class="form-control" value="{{ Auth::user()->cardNumber }}">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Gender')}}</label>
                    </div>
                    <div class="col-auto pt-2">
                        <div class="form-check form-check-inline ms-3">
                            <input class="form-check-input" type="radio" name="gender" id="man" value="man" {{(Auth::user()->gender === 'man' ? 'checked' : '')}}>
                            <label class="form-check-label" for="man">Man</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="gender" id="woman" value="woman" {{(Auth::user()->gender === 'woman' ? 'checked' : '')}}>
                            <label class="form-check-label" for="woman">Woman</label>
                          </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Phone')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="phone" class="form-control" value="{{ Auth::user()->phone }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Date of Birth')}}</label>
                    </div>
                    <div class="col">
                        <input type="date" name="birthday" class="form-control" value="{{ Auth::user()->birthday }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('City')}}</label>
                    </div>
                    <div class="col">
                        <input type="text" name="city" class="form-control" value="{{ Auth::user()->city }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-auto">
                        <label class="col-form-label">{{__('Repeat password')}}</label>
                    </div>
                    <div class="col">
                        <input type="password" name="repeatPassword" class="form-control" id="repeatPassword">
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary" id="submit">{{__('Save')}}</button>
                </div>
            </div>
        </div>
    </form>
        
</div>
<script>
$('#repeatPassword, #password').on('change', function() {
    password = $('#password').val();
    repeatPassword = $('#repeatPassword').val();
    console.log(password)
    if(password == '' || password === repeatPassword){
        $('#submit').prop('disabled',false);
        $('#repeatPassword').removeClass('bg-danger');
    }
    else {
        $('#submit').prop('disabled',true);
        $('#repeatPassword').addClass('bg-danger');
    }
    
})
</script>
@endsection