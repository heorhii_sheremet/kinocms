@extends('layout')
@section('content')
<div class="row">
    <div class="col-9">
        <div class="container">
            <div class="text-center display-6 mb-4">
                {{__('Mobile app')}}
            </div>
            <div class="row align-items-start">
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id facilis sequi incidunt a molestiae, iusto minima.
                Autem quod odio nesciunt, fugit dolores alias vitae at obcaecati, eum sed voluptate ex!
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id facilis sequi incidunt a molestiae, iusto minima.
                Autem quod odio nesciunt, fugit dolores alias vitae at obcaecati, eum sed voluptate ex!
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id facilis sequi incidunt a molestiae, iusto minima.
                Autem quod odio nesciunt, fugit dolores alias vitae at obcaecati, eum sed voluptate ex!
                <div class="col-3 mt-3">
                    <img src="{{asset('default-images/mobile-app.jpeg')}}" class="img-fluid" alt="{{__('Image')}}">
                </div>
                <div class="col-3 mt-3">
                    <img src="{{asset('default-images/mobile-app.jpeg')}}" class="img-fluid" alt="{{__('Image')}}">
                </div>
                <div class="col-3 mt-3">
                    <img src="{{asset('default-images/mobile-app.jpeg')}}" class="img-fluid" alt="{{__('Image')}}">
                </div>
                <div class="col-3 mt-3">
                    <img src="{{asset('default-images/mobile-app.jpeg')}}" class="img-fluid" alt="{{__('Image')}}">
                </div>
                <div class="row mt-3">
                    <div class="col-2">
                        <img src="{{asset('default-images/appStore.png')}}" class="img-fluid" alt="{{__('Image')}}">
                    </div>
                    <div class="col-2">
                        <img src="{{asset('default-images/GooglePlay.png')}}" class="img-fluid" alt="{{__('Image')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-3 bg-lght pt-5">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id facilis sequi incidunt a molestiae, iusto minima. Autem quod odio nesciunt, fugit dolores alias vitae at obcaecati, eum sed voluptate ex!
    </div>

</div>
@endsection