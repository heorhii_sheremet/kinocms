@extends('layout')
@section('content')
<div class="container">
    @if($pictures->count() > 0)
        <div class="sim-slider bg-transparent" id="first">
            <ul class="sim-slider-list">
                <li><img class="slide" src="http://pvbk.spb.ru/inc/slider/imgs/no-image.gif" alt="screen"></li> <!-- это экран -->
                @foreach($pictures as $picture)
                    @if($picture->slider === "home_top")
                        <li class="sim-slider-element">
                            <a href="{{$picture->url}}">
                                <img class="slide" src="{{$picture->img !== '' ? asset("$picture->img") : asset(config("app.defaultPicture"))}}" alt="0">
                            </a>
                            <div class="text text-break display-3 text-primary">{{$picture->text}}</div>
                        </li>
                    @endif
                @endforeach
            </ul>
            <div class="sim-slider-arrow-left"></div>
            <div class="sim-slider-arrow-right"></div>
            <div class="sim-slider-dots"></div>
        </div>
    @endif
    <div class="row text-center">
        <div class="display-4">{{__('Show today')}}</div>
    </div>
    <div class="row justify-content-start bg-secondary pt-4 ps-4">
        @foreach($films as $film)
            @if($film->status === 'curent')
                <div class="col-auto mb-4">
                    <div class="card" style="width: 18rem; height: 25rem;">
                        <img src="{{ $film->mainPicture !== '' ? asset("$film->mainPicture") : asset(config("app.defaultPicture")) }}" class="card-img-top" alt="Image Film" style="width: 18rem; height: 22rem;">
                        <div class="card-body text-center">
                        <a href="{{route('films.show', ['film' => $film->idFilm])}}">
                            <p class="card-text">{{$film->name}}</p>
                        </a>
                        <a class="btn btn-success mt-2" href="{{route('timetable', ['film' => $film->idFilm])}}">
                            <p class="card-text">{{__('Buy ticket')}}</p>
                        </a>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
    <div class="row text-center">
        <div class="display-4">{{__('Show soon')}}</div>
    </div>
    <div class="row justify-content-start bg-secondary pt-4 ps-4">
        @foreach($films as $film)
            @if($film->status === 'soon')
                <div class="col-auto mb-4">
                    <div class="card" style="width: 18rem; height: 25rem;">
                        <img src="{{ $film->mainPicture !== '' ? asset("$film->mainPicture") : asset(config("app.defaultPicture")) }}" class="card-img-top" alt="Image Film" style="width: 18rem; height: 22rem;">
                        <div class="card-body text-center">
                            <a href="{{route('film', ['id' => $film->idFilm])}}">
                                <p class="card-text">{{$film->name}}</p>
                            </a>
                            <a class="btn btn-success mt-2" href="{{route('timetable', ['film' => $film->idFilm])}}">
                                <p class="card-text">{{__('Buy ticket')}}</p>
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
    <div class="row text-center">
        <div class="display-4">{{__('News and Stock')}}</div>
    </div>
    @if($pictures->count() > 0)
        <div class="sim-slider bg-transparent" id="second">
            <ul class="sim-slider-list">
                <li><img class="slide" src="http://pvbk.spb.ru/inc/slider/imgs/no-image.gif" alt="screen"></li> <!-- это экран -->
                @foreach($pictures as $picture)
                    @if($picture->slider === "home_news")
                        <li class="sim-slider-element">
                            <a href="{{$picture->url}}">
                                <img class="slide" src="{{$picture->img !== '' ? asset("$picture->img") : asset(config("app.defaultPicture"))}}" alt="0">
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <div class="sim-slider-arrow-left"></div>
            <div class="sim-slider-arrow-right"></div>
            <div class="sim-slider-dots"></div>
        </div>
    @endif
    <div class="row text-center">
        <div class="container text-center form-control">{{__('Advertising')}}</div>
    </div>
    <div class="row text-center mt-5">
        <div class="display-2S">{{$data->seoText}}</div>
    </div>
</div>





















<script>
    function SimTop(sldrId) {

let id = document.getElementById(sldrId);
if(id) {
    this.sldrRoot = id
}
else {
    this.sldrRoot = document.querySelector('.sim-slider')
};

// Carousel objects
this.sldrList = this.sldrRoot.querySelector('.sim-slider-list');
this.sldrElements = this.sldrList.querySelectorAll('.sim-slider-element');
this.sldrElemFirst = this.sldrList.querySelector('.sim-slider-element');
this.leftArrow = this.sldrRoot.querySelector('div.sim-slider-arrow-left');
this.rightArrow = this.sldrRoot.querySelector('div.sim-slider-arrow-right');
this.indicatorDots = this.sldrRoot.querySelector('div.sim-slider-dots');

// Initialization
this.options = SimTop.defaults;
SimTop.initialize(this)
};

SimTop.defaults = {

// Default options for the carousel
loop: true,     // Бесконечное зацикливание слайдера
auto: true,     // Автоматическое пролистывание
interval: {{$data->topRotationalSpeed}}*1000, // Интервал между пролистыванием элементов (мс)
arrows: true,   // Пролистывание стрелками
dots: true      // Индикаторные точки
};

SimTop.prototype.elemPrev = function(num) {
num = num || 1;

let prevElement = this.currentElement;
this.currentElement -= num;
if(this.currentElement < 0) this.currentElement = this.elemCount-1;

if(!this.options.loop) {
    if(this.currentElement == 0) {
        this.leftArrow.style.display = 'none'
    };
    this.rightArrow.style.display = 'block'
};

this.sldrElements[this.currentElement].style.opacity = '1';
this.sldrElements[prevElement].style.opacity = '0';

if(this.options.dots) {
    this.dotOn(prevElement); this.dotOff(this.currentElement)
}
};

SimTop.prototype.elemNext = function(num) {
num = num || 1;

let prevElement = this.currentElement;
this.currentElement += num;
if(this.currentElement >= this.elemCount) this.currentElement = 0;

if(!this.options.loop) {
    if(this.currentElement == this.elemCount-1) {
        this.rightArrow.style.display = 'none'
    };
    this.leftArrow.style.display = 'block'
};

this.sldrElements[this.currentElement].style.opacity = '1';
this.sldrElements[prevElement].style.opacity = '0';

if(this.options.dots) {
    this.dotOn(prevElement); this.dotOff(this.currentElement)
}
};

SimTop.prototype.dotOn = function(num) {
this.indicatorDotsAll[num].style.cssText = 'background-color:#BBB; cursor:pointer;'
};

SimTop.prototype.dotOff = function(num) {
this.indicatorDotsAll[num].style.cssText = 'background-color:#556; cursor:default;'
};

SimTop.initialize = function(that) {

// Constants
that.elemCount = that.sldrElements.length; // Количество элементов

// Variables
that.currentElement = 0;
let bgTime = getTime();

// Functions
function getTime() {
    return new Date().getTime();
};
function setAutoScroll() {
    that.autoScroll = setInterval(function() {
        let fnTime = getTime();
        if(fnTime - bgTime + 10 > that.options.interval) {
            bgTime = fnTime; that.elemNext()
        }
    }, that.options.interval)
};


// Start initialization
if(that.elemCount <= 1) {   // Отключить навигацию
    that.options.auto = false; that.options.arrows = false; that.options.dots = false;
    that.leftArrow.style.display = 'none'; that.rightArrow.style.display = 'none'
};
if(that.elemCount >= 1) {   // показать первый элемент
    that.sldrElemFirst.style.opacity = '1';
};

if(!that.options.loop) {
    that.leftArrow.style.display = 'none';  // отключить левую стрелку
    that.options.auto = false; // отключить автопркрутку
}
else if(that.options.auto) {   // инициализация автопрокруки
    setAutoScroll();
    // Остановка прокрутки при наведении мыши на элемент
    that.sldrList.addEventListener('mouseenter', function() {clearInterval(that.autoScroll)}, false);
    that.sldrList.addEventListener('mouseleave', setAutoScroll, false)
};

if(that.options.arrows) {  // инициализация стрелок
    that.leftArrow.addEventListener('click', function() {
        let fnTime = getTime();
        if(fnTime - bgTime > 1000) {
            bgTime = fnTime; that.elemPrev()
        }
    }, false);
    that.rightArrow.addEventListener('click', function() {
        let fnTime = getTime();
        if(fnTime - bgTime > 1000) {
            bgTime = fnTime; that.elemNext()
        }
    }, false)
}
else {
    that.leftArrow.style.display = 'none'; that.rightArrow.style.display = 'none'
};

if(that.options.dots) {  // инициализация индикаторных точек
    let sum = '', diffNum;
    for(let i=0; i<that.elemCount; i++) {
        sum += '<span class="sim-dot"></span>'
    };
    that.indicatorDots.innerHTML = sum;
    that.indicatorDotsAll = that.sldrRoot.querySelectorAll('span.sim-dot');
    // Назначаем точкам обработчик события 'click'
    for(let n=0; n<that.elemCount; n++) {
        that.indicatorDotsAll[n].addEventListener('click', function() {
            diffNum = Math.abs(n - that.currentElement);
            if(n < that.currentElement) {
                bgTime = getTime(); that.elemPrev(diffNum)
            }
            else if(n > that.currentElement) {
                bgTime = getTime(); that.elemNext(diffNum)
            }
            // Если n == that.currentElement ничего не делаем
        }, false)
    };
    that.dotOff(0);  // точка[0] выключена, остальные включены
    for(let i=1; i<that.elemCount; i++) {
        that.dotOn(i)
    }
}
};

function SimNews(sldrId) {

let id = document.getElementById(sldrId);
if(id) {
    this.sldrRoot = id
}
else {
    this.sldrRoot = document.querySelector('.sim-slider')
};

// Carousel objects
this.sldrList = this.sldrRoot.querySelector('.sim-slider-list');
this.sldrElements = this.sldrList.querySelectorAll('.sim-slider-element');
this.sldrElemFirst = this.sldrList.querySelector('.sim-slider-element');
this.leftArrow = this.sldrRoot.querySelector('div.sim-slider-arrow-left');
this.rightArrow = this.sldrRoot.querySelector('div.sim-slider-arrow-right');
this.indicatorDots = this.sldrRoot.querySelector('div.sim-slider-dots');

// Initialization
this.options = SimNews.defaults;
SimNews.initialize(this)
};

SimNews.defaults = {

// Default options for the carousel
loop: true,     // Бесконечное зацикливание слайдера
auto: true,     // Автоматическое пролистывание
interval: {{$data->newsRotationalSpeed}}*1000, // Интервал между пролистыванием элементов (мс)
arrows: true,   // Пролистывание стрелками
dots: true      // Индикаторные точки
};

SimNews.prototype.elemPrev = function(num) {
num = num || 1;

let prevElement = this.currentElement;
this.currentElement -= num;
if(this.currentElement < 0) this.currentElement = this.elemCount-1;

if(!this.options.loop) {
    if(this.currentElement == 0) {
        this.leftArrow.style.display = 'none'
    };
    this.rightArrow.style.display = 'block'
};

this.sldrElements[this.currentElement].style.opacity = '1';
this.sldrElements[prevElement].style.opacity = '0';

if(this.options.dots) {
    this.dotOn(prevElement); this.dotOff(this.currentElement)
}
};

SimNews.prototype.elemNext = function(num) {
num = num || 1;

let prevElement = this.currentElement;
this.currentElement += num;
if(this.currentElement >= this.elemCount) this.currentElement = 0;

if(!this.options.loop) {
    if(this.currentElement == this.elemCount-1) {
        this.rightArrow.style.display = 'none'
    };
    this.leftArrow.style.display = 'block'
};

this.sldrElements[this.currentElement].style.opacity = '1';
this.sldrElements[prevElement].style.opacity = '0';

if(this.options.dots) {
    this.dotOn(prevElement); this.dotOff(this.currentElement)
}
};

SimNews.prototype.dotOn = function(num) {
this.indicatorDotsAll[num].style.cssText = 'background-color:#BBB; cursor:pointer;'
};

SimNews.prototype.dotOff = function(num) {
this.indicatorDotsAll[num].style.cssText = 'background-color:#556; cursor:default;'
};

SimNews.initialize = function(that) {

// Constants
that.elemCount = that.sldrElements.length; // Количество элементов

// Variables
that.currentElement = 0;
let bgTime = getTime();

// Functions
function getTime() {
    return new Date().getTime();
};
function setAutoScroll() {
    that.autoScroll = setInterval(function() {
        let fnTime = getTime();
        if(fnTime - bgTime + 10 > that.options.interval) {
            bgTime = fnTime; that.elemNext()
        }
    }, that.options.interval)
};


// Start initialization
if(that.elemCount <= 1) {   // Отключить навигацию
    that.options.auto = false; that.options.arrows = false; that.options.dots = false;
    that.leftArrow.style.display = 'none'; that.rightArrow.style.display = 'none'
};
if(that.elemCount >= 1) {   // показать первый элемент
    that.sldrElemFirst.style.opacity = '1';
};

if(!that.options.loop) {
    that.leftArrow.style.display = 'none';  // отключить левую стрелку
    that.options.auto = false; // отключить автопркрутку
}
else if(that.options.auto) {   // инициализация автопрокруки
    setAutoScroll();
    // Остановка прокрутки при наведении мыши на элемент
    that.sldrList.addEventListener('mouseenter', function() {clearInterval(that.autoScroll)}, false);
    that.sldrList.addEventListener('mouseleave', setAutoScroll, false)
};

if(that.options.arrows) {  // инициализация стрелок
    that.leftArrow.addEventListener('click', function() {
        let fnTime = getTime();
        if(fnTime - bgTime > 1000) {
            bgTime = fnTime; that.elemPrev()
        }
    }, false);
    that.rightArrow.addEventListener('click', function() {
        let fnTime = getTime();
        if(fnTime - bgTime > 1000) {
            bgTime = fnTime; that.elemNext()
        }
    }, false)
}
else {
    that.leftArrow.style.display = 'none'; that.rightArrow.style.display = 'none'
};

if(that.options.dots) {  // инициализация индикаторных точек
    let sum = '', diffNum;
    for(let i=0; i<that.elemCount; i++) {
        sum += '<span class="sim-dot"></span>'
    };
    that.indicatorDots.innerHTML = sum;
    that.indicatorDotsAll = that.sldrRoot.querySelectorAll('span.sim-dot');
    // Назначаем точкам обработчик события 'click'
    for(let n=0; n<that.elemCount; n++) {
        that.indicatorDotsAll[n].addEventListener('click', function() {
            diffNum = Math.abs(n - that.currentElement);
            if(n < that.currentElement) {
                bgTime = getTime(); that.elemPrev(diffNum)
            }
            else if(n > that.currentElement) {
                bgTime = getTime(); that.elemNext(diffNum)
            }
            // Если n == that.currentElement ничего не делаем
        }, false)
    };
    that.dotOff(0);  // точка[0] выключена, остальные включены
    for(let i=1; i<that.elemCount; i++) {
        that.dotOn(i)
    }
}
};

new SimTop('first');
new SimNews('second');
</script>
<style>
   
    .slide {
        width: 100%; !important;
        height: 600px;
    }

    .sim-slider {
    max-width: 1000px;
    min-width: 320px;
    margin: 20px auto;
    padding: 30px 50px;
    background-color: white;
    }

    /* General styles */
    .sim-slider {
    position: relative;
    }

    .sim-slider-list {
    margin: 0;
    padding: 0;
    list-style-type: none;
    position: relative;
    }

    .sim-slider-element {
    width: 100%;
    transition: opacity 1s ease-in;
    opacity: 0;
    position: absolute;
    z-index: 2;
    left: 0;
    top: 0;
    display: block;
    }

    .text {
       
        position: absolute;
        left: 10px;
        top: 100px;
    }

    /* Navigation item styles */
    div.sim-slider-arrow-left,
    div.sim-slider-arrow-right {
    width: 22px;
    height: 40px;
    position: absolute;
    cursor: pointer;
    opacity: 0.6;
    z-index: 4;
    }

    div.sim-slider-arrow-left {
    left: 10px;
    top: 40%;
    display: block;
    background: url("http://pvbk.spb.ru/inc/slider/sim-files/sim-arrow-left.png") no-repeat;
    }

    div.sim-slider-arrow-right {
    right: 10px;
    top: 40%;
    display: block;
    background: url("http://pvbk.spb.ru/inc/slider/sim-files/sim-arrow-right.png") no-repeat;
    }

    div.sim-slider-arrow-left:hover {
    opacity: 1.0;
    }

    div.sim-slider-arrow-right:hover {
    opacity: 1.0;
    }

    div.sim-slider-dots {
    width: 100%;
    height: auto;
    position: absolute;
    left: 0;
    bottom: 0;
    z-index: 3;
    text-align: center;
    }

    span.sim-dot {
    width: 10px;
    height: 10px;
    margin: 5px 7px;
    padding: 0;
    display: inline-block;
    background-color: #BBB;
    border-radius: 5px;
    cursor: pointer;
    }
</style>
@endsection