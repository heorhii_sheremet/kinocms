@extends('layout')
@section('content')
<div class="row">
    <div class="col-9">
       <div class="text-center display-6 mb-4">
            {{__('Contacts')}}
        </div>
        <div class="row m-1">
            @foreach($contacts as $contact)
            @if($contact->switch == '1')
            <div class="col-12 gx-5 mb-5">
                <div class="row">
                    <div class="col-6">
                        <div class="row display-6">
                            {{$contact->name}}
                        </div>
                        <div class="row text-break">
                            {{$contact->address}}
                            @foreach(preg_split("/[\r\n]+/", $contact->address) as $str)
                            {{$str}} <br>
                            @endforeach
                        </div>
                        <div class="row justify-content-center">
                            <img src="{{$contact->logo}}" alt="{{__('Logo')}}" height="400" style="width: auto">
                        </div>
                    </div>
                    <div class="col-6 mt-3 border border-primary">
                        <iframe src="{{$contact->coordinates}}" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>

    <div class="col-3 bg-lght pt-5">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id facilis sequi incidunt a molestiae, iusto minima. Autem quod odio nesciunt, fugit dolores alias vitae at obcaecati, eum sed voluptate ex!
    </div>

</div>
@endsection