<?php

use App\Http\Controllers\NewsController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\StockController;
use App\Http\Controllers\CinemaController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\TimetableController;
use App\Http\Controllers\FilmCardController;
use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\PosterController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\HomePageController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\PageController as AdminPageController;
use App\Http\Controllers\Admin\StockController as AdminStockController;
use App\Http\Controllers\Admin\NewsController as AdminNewsController;
use App\Http\Controllers\Admin\SessionController;
use App\Http\Controllers\Admin\HallController;
use App\Http\Controllers\Admin\CinemaController as AdminCinemaController;
use App\Http\Controllers\Admin\BannersController;
use App\Http\Controllers\Admin\FilmController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\IndexController;
use App\Http\Controllers\Admin\MailingListController;
use League\CommonMark\Extension\CommonMark\Node\Block\IndentedCode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/poster', [PosterController::class, 'index'])->name('poster');
Route::get('/localization/{lang}', [LocalizationController::class, 'index'])->name('localization');
Route::get('/film/{id}', [FilmCardController::class, 'index'])->name('film');
Route::get('/timetable', [TimetableController::class, 'index'])->name('timetable');
Route::get('/booking/{idSession}', [TimetableController::class, 'bookingTicket'])->name('booking');
Route::post('/booking/save', [TimetableController::class, 'saveBooking'])->name('booking.save')->middleware('auth');;
Route::get('/registration', [RegistrationController::class, 'registration'])->name('registration');
Route::post('/user/save', [RegistrationController::class, 'userSave'])->name('userSave');
Route::get('/login', [RegistrationController::class, 'login'])->name('login');
Route::post('/login/auth', [RegistrationController::class, 'auth'])->name('login.auth');
Route::get('/account', [RegistrationController::class, 'account'])->name('account');
Route::put('/account/save', [RegistrationController::class, 'accountSave'])->name('account.save');
Route::get('/logout', [RegistrationController::class, 'logout'])->name('logout');
Route::get('/cinemas', [CinemaController::class, 'index'])->name('cinemas');
Route::get('/cinema/{cinema}', [CinemaController::class, 'show'])->name('cinema.show');
Route::get('/hall/{hall}', [CinemaController::class, 'showHall'])->name('hall.show');
Route::get('/stock', [StockController::class, 'index'])->name('stock');
Route::get('/stock/{stock}', [StockController::class, 'show'])->name('stock.show');
Route::get('/news', [NewsController::class, 'index'])->name('news');
Route::get('/news/{news}', [NewsController::class, 'show'])->name('news.show');
Route::get('/contacts', [PageController::class, 'contacts'])->name('contacts');
Route::get('/mobile-app', [PageController::class, 'mobileApp'])->name('mobile-app');
Route::get('/page/{page}', [PageController::class, 'index'])->name('page');




Route::group(['prefix' => 'admin', 'middleware' => 'isAdmin'], function()
{
    Route::get('/', IndexController::class);
    Route::get('/banners', [BannersController::class, 'index'])->name("banners");
    Route::post('/banners/save/slider/top', [BannersController::class, 'saveSliderTop'])->name("save.slider.top");
    Route::post('/banners/save/slider/news', [BannersController::class, 'saveSliderNews'])->name("save.slider.news");
    Route::post('/banners/save/rotational-speed/top', [BannersController::class, 'saveRotationalSpeedTop'])->name("save.rotational-speed.top");
    Route::post('/banners/save/banner', [BannersController::class, 'saveBanner'])->name("save.banner");
    Route::get('/banners/delete/banner', [BannersController::class, 'deleteBanner'])->name("delete.banner");
    Route::resource('films', FilmController::class);
    Route::resource('films.sessions', SessionController::class)->shallow();
    Route::resource('cinemas', AdminCinemaController::class)->except(['show']);
    Route::resource('cinemas.halls', HallController::class)->shallow();
    Route::resource('news', AdminNewsController::class)->except(['show']);
    Route::resource('stock', AdminStockController::class)->except(['show']);
    Route::resource('pages', AdminPageController::class);
    Route::resource('contacts', ContactController::class)->only(['index', 'store']);
    Route::resource('home-page', HomePageController::class)->only(['index', 'store']);
    Route::resource('users', UserController::class)->except(['show','create', 'store']);
    Route::get('/mailing', [MailingListController::class, 'index'])->name('mailing.index');
    Route::post('/mailing/send', [MailingListController::class, 'send'])->name('mailing.send');
    Route::get('/mailing/getQueueCount', [MailingListController::class, 'getQueueCount'])->name('mailing.getQueueCount');
});



