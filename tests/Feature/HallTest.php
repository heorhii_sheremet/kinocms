<?php

namespace Tests\Feature;

use App\Models\CinemaModel;
use App\Models\HallModel;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HallTest extends TestCase
{
    public function testSaveHall()
    {   
        $cinema = CinemaModel::all()->last();
        $this->actingAs(User::find(1));
        $this->post(route('cinemas.halls.store', ['cinema' => $cinema->idCinema]), ['number' => 22, 'idCinema' => $cinema->idCinema]);
       
        $hall = HallModel::all()->last();
       
        $this->assertTrue($hall->number === 22);
    }

    public function testUpdateHall()
    {
        $hall = HallModel::all()->last();
        $this->actingAs(User::find(1))->put(route('halls.update', ['hall' => $hall->idHall]), ['number' => 221]);
        
        $hall = HallModel::all()->last();
       
        $this->assertTrue($hall->number === 221);
    }

    public function testShowHall()
    {
        $hall = HallModel::all()->last();
        $response = $this->get(route('hall.show', ['hall' => $hall->idHall]));
        $response->assertViewIs('hall-card');
        $response->assertViewHas('hall');
    }

    public function testDeleteHall()
    {
        $hall = HallModel::all()->last();
        $id = $hall->idHall;

        $this->actingAs(User::find(1))->delete(route('halls.destroy', ['hall' => $id]));

        $hall = HallModel::find($id);
    
        $this->assertTrue($hall === null);
    }

    public function testDeleteCinemaWithHall()
    {
        $this->actingAs(User::find(1));
        $this->post(route('cinemas.store'), ['name' => 'cinema Test Store']);
       
        $cinema = CinemaModel::all()->last();

        $this->actingAs(User::find(1));
        $this->post(route('cinemas.halls.store', ['cinema' => $cinema->idCinema]), ['number' => 1, 'idCinema' => $cinema->idCinema]);
       
        $hall = HallModel::all()->last();
        
        $this->actingAs(User::find(1))->delete(route('cinemas.destroy', ['cinema' => $cinema->idCinema]));

        $hall = HallModel::find($hall->idHall);

        $this->assertTrue($hall === null);
    }
}
