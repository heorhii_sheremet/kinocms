<?php

namespace Tests\Feature;

use App\Models\CinemaModel;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CinemaTest extends TestCase
{
    public function testSaveCinema()
    {   

        $this->actingAs(User::find(1));
        $this->post(route('cinemas.store'), ['name' => 'cinema Test Store']);
       
        $cinema = CinemaModel::all()->last();
       
        $this->assertTrue($cinema->name === 'cinema Test Store');
    }

    public function testUpdateCinema()
    {
        $cinema = CinemaModel::all()->last();
        $this->actingAs(User::find(1))->put(route('cinemas.update', ['cinema' => $cinema->idCinema]), ['name' => 'cinema Test Udate']);
        
        $cinema = CinemaModel::all()->last();
       
        $this->assertTrue($cinema->name === 'cinema Test Udate');
    }

    public function testShowCinema()
    {
        $cinema = CinemaModel::all()->last();
        $response = $this->get(route('cinema.show', ['cinema' => $cinema->idCinema]));
        $response->assertViewIs('cinema-card');
        $response->assertViewHas('cinema');
    }

    public function testDeleteCinema()
    {
        $cinema = CinemaModel::all()->last();
        $id = $cinema->idCinema;

        $this->actingAs(User::find(1))->delete(route('cinemas.destroy', ['cinema' => $id]));

        $cinema = CinemaModel::all()->last();
        $lastId =$cinema->idCinema;
        $this->assertTrue($id !== $lastId);
    }
}
