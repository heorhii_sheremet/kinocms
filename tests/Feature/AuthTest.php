<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */


    public function testAuth()
    {
        $response = $this->post('/login/auth',['email' => 'i@g.com', 'password' => '123']);
        $response->assertRedirect(route('home'));
    }

    public function testAuthAdmin()
    {
        $this->post('/login/auth',['email' => User::find(1)->email, 'password' => '123']);
        //$response->assertRedirect('admin');
        $this->assertAuthenticatedAs(User::find(1));
    }

}
