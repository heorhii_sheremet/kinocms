<?php

namespace Tests\Feature;

use App\Models\FilmModel;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FilmTest extends TestCase
{

    public function testSaveFilm()
    {   

        $this->actingAs(User::find(1));
        $this->post(route('films.store'), ['name' => 'test2', "price" => '1303']);
        $film = FilmModel::all()->last();

        $this->assertTrue($film->name === 'test2' && $film->price == 1303);
    }

    public function testUpdateFilm()
    {
        $film = FilmModel::all()->last();
        $this->actingAs(User::find(1))->put(route('films.update', ['film' => $film->idFilm]), ['name' => 'test3', "price" => '12']);

        $film = FilmModel::all()->last();

        $this->assertTrue($film->name === 'test3' && $film->price == 12);
    }

    public function testShowFilm()
    {
        $film = FilmModel::all()->last();
        $response = $this->get(route('film', ['id' => $film->idFilm]));
        $response->assertViewIs('film-card');
        $response->assertViewHas('film');
    }

    public function testDeleteFilm()
    {
        $film = FilmModel::all()->last();
        $id = $film->idFilm;

        $this->actingAs(User::find(1))->delete(route('films.destroy', ['film' => $id]));

        $film = FilmModel::all()->last();
        $lastId =$film->idFilm;
        $this->assertTrue($id !== $lastId);
    }
}
